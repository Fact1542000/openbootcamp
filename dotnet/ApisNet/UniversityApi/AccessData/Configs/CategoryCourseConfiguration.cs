using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UniversityApi.Models.DataModels;

namespace UniversityApi.AccessData.Configs;
public class CategoryCourseConfiguration : IEntityTypeConfiguration<CategoryCourse>
{
    public void Configure(EntityTypeBuilder<CategoryCourse> builder)
    {
        builder.HasKey(sc => new {sc.CategoryId, sc.CourseId});
    }
}
