namespace UniversityApi.Models.DataModels;
public class CategoryCourse
{
    public int CategoryId { get; set; }
    public Category Category { get; set; } = null!;
    public int CourseId { get; set; }
    public Course Course { get; set; } = null!;
}