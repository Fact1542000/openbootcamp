using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UniversityApi.Helpers;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Category;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Enums;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Controllers;
[Route("api/[controller]")]
[ApiController]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin)+","+nameof(Rol.User))]
public class CategoryController : ControllerBase
{
    private readonly SaveChangeHandle _saveChange;
    private readonly ILogger<CategoryController> _logger;
    private readonly ICategoryService _categoryService;

    public CategoryController(SaveChangeHandle saveChange, ICategoryService categoryService, ILogger<CategoryController> logger)
    {
        _saveChange = saveChange;
        _categoryService = categoryService;
        _logger = logger;
    }

    #region READ
    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetCategoryDto>>> GetAllCategories()
    {
        try
        {
            _logger.LogInformation($"{nameof(CategoryController)}::{nameof(this.GetAllCategories)} - Loading data...");
            var categories = await _categoryService.GetAllAsync();
            return categories.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogInformation($"{nameof(CategoryController)}::{nameof(this.GetAllCategories)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("{id}", Name = "GetCategoryById")]
    public async Task<ActionResult<GetCategoryDto>> GetCategoryById(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(CategoryController)}::{nameof(this.GetCategoryById)} - Loading data...");
            var category = await _categoryService.GetByIdAsync(id);

            if (category is null)
            {
                _logger.LogWarning($"{nameof(CategoryController)}::{nameof(this.GetCategoryById)} - Return:: NotFound");
                return NotFound($"Could not found the category with the id::{id}");
            }

            return category;
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CategoryController)}::{nameof(this.GetCategoryById)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetAllCourseRelated/{id}")]
    public async Task<ActionResult<IEnumerable<GetCourseDto>>> GetAllCoursesRelated(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(CategoryController)}::{nameof(this.GetAllCoursesRelated)} - Loading data...");
            var courses = await _categoryService.GetAllCourseRelatedWithCategory(id);
            if (courses is null)
            {
                _logger.LogWarning($"{nameof(CategoryController)}::{nameof(this.GetAllCoursesRelated)} - Return:: NotFound");
                return NotFound("This category dont have courses related or the category is not found");
            }

            return courses.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CategoryController)}::{nameof(this.GetAllCoursesRelated)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region WRITE
    [HttpPost]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> PostCategory([FromBody] PostCategoryDto categoryDto)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(CategoryController)}::{nameof(this.PostCategory)} - Invalid Model");
                return BadRequest("Invalid category");
            }

            var category = await _categoryService.AddAsync(categoryDto);

            if (category is null)
            {
                _logger.LogWarning($"{nameof(CategoryController)}::{nameof(this.PostCategory)} - Return:: BadRequest");
                return BadRequest("Could not insert the category");
            }

            return RedirectToRoute(nameof(GetCategoryById), new { id = category.Id });
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CategoryController)}::{nameof(this.PostCategory)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpPost("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> AddCategoryCourse(int id, AddCategoryCourseDto categoryCourseDto)
    {
        try
        {
            var category = await _categoryService.GetByIdAsync(id);

            if (category is null)
            {
                _logger.LogWarning($"{nameof(CategoryController)}::{nameof(this.AddCategoryCourse)} - Return:: NotFound");
                return NotFound("Could not found the category");
            }

            await _categoryService.AddCourse(id, categoryCourseDto);

            return await _saveChange.Handle<Category>(action: "CategoryCourse added successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CategoryController)}::{nameof(this.AddCategoryCourse)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpPut("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> PutCategory(int id, [FromBody] PutCategoryDto categoryDto)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(CategoryController)}::{nameof(this.PutCategory)} - Ivalid model");
                return BadRequest("Invalid category");
            }

            await _categoryService.EditAsync(id, categoryDto);

            return await _saveChange.Handle<Category>(action: "Category edit successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CategoryController)}::{nameof(this.PutCategory)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpDelete("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> DeleteCategory(int id)
    {
        try
        {
            await _categoryService.DeleteAsync(id);
            return await _saveChange.Handle<Category>(action: "Category delete successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CategoryController)}::{nameof(this.DeleteCategory)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion
}