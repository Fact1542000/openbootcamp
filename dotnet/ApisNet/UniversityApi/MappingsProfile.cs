using AutoMapper;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Category;
using UniversityApi.Models.Dtos.Chapter;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Dtos.Student;
using UniversityApi.Models.Dtos.User;

namespace UniversityApi;

public class MappingsProfile : Profile
{
    public MappingsProfile()
    {
        // Student mappings
        CreateMap<PostStudentDto, Student>();
        CreateMap<PutStudentDto, Student>();
        CreateMap<Student, GetStudentDto>();

        // Course mappings
        CreateMap<PostCourseDto, Course>();
        CreateMap<PutCourseDto, Course>();
        CreateMap<Course, GetCourseDto>();

        // User mappings
        CreateMap<PostUserDto, User>();
        CreateMap<PutCourseDto, User>();
        CreateMap<User, GetUserDto>()
                .ForMember(dest => dest.Rol, opt => opt.MapFrom(src => src.Rol.ToString()));

        // Chapter mappings
        CreateMap<PostChapterDto, Chapter>();
        CreateMap<PutChapterDto, Chapter>();
        CreateMap<Chapter, GetChapterDto>();

        // Category mappings
        CreateMap<PostCategoryDto, Category>();
        CreateMap<PutCategoryDto, Category>();
        CreateMap<Category, GetCategoryDto>();
    }
}