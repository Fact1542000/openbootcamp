using System.ComponentModel.DataAnnotations;
using UniversityApi.Models.Enums;

namespace UniversityApi.Models.Dtos.User;

public class PostUserDto
{
    [Required]
    public string Name { get; set; } = null!;
    [Required]
    public string LastName { get; set; } = null!;
    [EmailAddress]
    public string Email { get; set; } = null!;
    [Required]
    public string Password { get; set; } = null!;
    [Required]
    public Rol Rol { get; set; }
    [Required]
    public DateTime CreatedAt { get; set; }
}