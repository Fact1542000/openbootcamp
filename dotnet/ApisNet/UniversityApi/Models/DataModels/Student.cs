namespace UniversityApi.Models.DataModels;

public class Student : BaseEntity
{
    public string Name { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public DateTime DateOfBirth { get; set; }
    public IList<CourseStudent> CourseStudents { get; set; } = null!;
}