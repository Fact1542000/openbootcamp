using System.ComponentModel.Design.Serialization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using UniversityApi.Models;
using UniversityApi.Models.Enums;

namespace UniversityApi.Helpers;

public static class JwtHelpers
{
    public static IEnumerable<Claim> GetClaims(this UserTokens userAccounts, Guid id)
    {
        var claims = new List<Claim>
        {
            new Claim("Id", userAccounts.Id.ToString()),
            new Claim(ClaimTypes.Name, userAccounts.UserName),
            new Claim(ClaimTypes.Email, userAccounts.EmailId),
            new Claim(ClaimTypes.NameIdentifier, id.ToString()),
            new Claim(ClaimTypes.Expiration, DateTime.UtcNow.AddDays(1).ToString("MMM dd yyyy HH:mm:ss tt"))
        };

        if (userAccounts.EmailId == "admin@hotmail.com")
        {
            claims.Add(new Claim(ClaimTypes.Role, nameof(Rol.Admin)));
        } else if (userAccounts.EmailId != "admin@hotmail.com")
        {
            claims.Add(new Claim(ClaimTypes.Role, nameof(Rol.User)));
            claims.Add(new Claim("UserOnly", $"User {userAccounts.Id}"));
        }

        return claims;
    }

    public static IEnumerable<Claim> GetClaims(this UserTokens userAccounts, out Guid Id)
    {
        Id = Guid.NewGuid();
        return GetClaims(userAccounts, Id);
    }

    public static UserTokens GenerateTokenKey(UserTokens model, JwtSettings jwtSettings)
    {
        try
        {
            var userToken = new UserTokens();
            if (model is null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            // Obtain SECRET KEY
            var key = System.Text.Encoding.ASCII.GetBytes(jwtSettings.IssuserSigningKey);

            Guid Id;

            // Expire in 1 day
            DateTime expireTime = DateTime.UtcNow.AddDays(1);

            // Validity of out token
            userToken.Validity = expireTime.TimeOfDay;

            // Generate OUT JWT
            var JwToken = new JwtSecurityToken(
                    issuer: jwtSettings.ValidIssuer,
                    audience: jwtSettings.ValidAudience,
                    claims: GetClaims(model, out Id),
                    notBefore: new DateTimeOffset(DateTime.Now).DateTime,
                    expires: new DateTimeOffset(expireTime).DateTime,
                    signingCredentials: new SigningCredentials(
                            new SymmetricSecurityKey(key),
                            SecurityAlgorithms.HmacSha256
                        )
                );

            userToken.Token = new JwtSecurityTokenHandler().WriteToken(JwToken);

            userToken.UserName = model.UserName;
            userToken.Id = model.Id;
            userToken.GuidId = Id;
            return userToken;
        }
        catch(Exception ex)
        {
            throw new Exception("Error Generating jwt", ex);
        }
    }
}