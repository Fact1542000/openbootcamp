using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UniversityApi.Helpers;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Dtos.Student;
using UniversityApi.Models.Enums;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin)+","+nameof(Rol.User))]
public class CourseController : ControllerBase
{
    private readonly SaveChangeHandle _saveChange;
    private readonly ILogger<CourseController> _logger;
    private readonly ICourseService _courseService;
    private readonly IMapper _mapper;

    public CourseController(SaveChangeHandle saveChange, ICourseService courseService, IMapper mapper, ILogger<CourseController> logger)
    {
        _saveChange = saveChange;
        _courseService = courseService;
        _mapper = mapper;
        _logger = logger;
    }

    #region READ
    [HttpGet("GetAllCoursesWithAllField")]
    public async Task<ActionResult<IEnumerable<GetCourseDto>>> GetAllCoursesWithAllField()
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetAllCoursesWithAllField)} - Loading data...");

            var courses = await _courseService.GetAllCourseWithAllField();

            if (courses is null)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.GetAllCoursesWithAllField)} - Return:: BadRequest");
                return BadRequest("Could not list all the courses");
            }

            return courses.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetAllCoursesWithAllField)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetCourseWithCategory/{id}")]
    public async Task<ActionResult<GetCourseDto>> GetCourseWithCategory(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetCourseWithCategory)} - Loading data...");
            var course = await _courseService.GetCourseWithCategory(id);
            if (course is null)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.GetCourseWithCategory)} - Return:: NotFound");
                return NotFound($"Could not found the course with the id::{id}");
            }

            return course;
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetCourseWithCategory)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetCourseWithChapter/{id}")]
    public async Task<ActionResult<GetCourseDto>> GetCourseWithChapter(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetCourseWithChapter)} - Loading data...");

            var course = await _courseService.GetCourseWithChapter(id);

            if (course is null)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.GetCourseWithChapter)} - Return:: NotFound");
                return NotFound($"Could not found the course with the id::{id}");
            }

            return course;
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetCourseWithChapter)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("WithOutSyllabus")]
    public async Task<ActionResult<IEnumerable<GetCourseDto>>> GetAllCourseWithoutSyllabus()
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetAllCourseWithoutSyllabus)} - Loading data...");

            var courses = await _courseService.GetAllCourseWithoutSyllabus();

            return courses.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetAllCourseWithoutSyllabus)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetAllCoursesWithStudents")]
    public async Task<ActionResult<IEnumerable<GetCourseDto>>> GetAllCoursesWithStudents()
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetAllCoursesWithStudents)} - Loading data...");

            var courses = await _courseService.GetAllCoursesWithStudents();

            return courses.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetAllCoursesWithStudents)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("WithSpecificCategory/{categoryId}")]
    public async Task<ActionResult<IEnumerable<GetCourseDto>>> GetCoursesWithSpecificCategory(int categoryId)
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetCoursesWithSpecificCategory)} - Loading data...");

            var courses = await _courseService.GetAllCourseWithSpecificCategory(categoryId);

            return courses.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetCoursesWithSpecificCategory)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetStudentsFromSpecificCourse/{id}")]
    public async Task<ActionResult<IEnumerable<GetStudentDto>>> GetStudentsFromSpecificCourse(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetStudentsFromSpecificCourse)} - Loading data...");

            var students = await _courseService.GetStudentsFromSpecificCourse(id);

            return students.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetStudentsFromSpecificCourse)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetSyllabusFromSpecificCourse/{id}")]
    public async Task<ActionResult<string>> GetSyllabusFromSpecifcCourse(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetSyllabusFromSpecifcCourse)} - Loading data...");

            return await _courseService.GetSyllabusFormSpecificCourse(id);
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetSyllabusFromSpecifcCourse)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetCourseDto>>> GetAllCourses()
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetAllCourses)} - Loading data...");

            var courses = await _courseService.GetAllAsync();

            return courses.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetAllCourses)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("{id}", Name = "GetCourseById")]
    public async Task<ActionResult<GetCourseDto>> GetCourseById(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(CourseController)}::{nameof(this.GetCourseById)} - Loading data...");

            var course = await _courseService.GetByIdAsync(id);

            if (course is null)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.GetCourseById)} - Return:: NotFoud");

                return NotFound($"Could not found the course with the id::{id}");
            }

            return course;
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.GetCourseById)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region WRITE
    [HttpPost]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> PostCourse([FromBody] PostCourseDto courseDto)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.PostCourse)} - Invalid model - Return:: BadRequest");
                return BadRequest("Invalid course");
            }

            var course = await _courseService.AddAsync(courseDto);

            if (course is null)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.PostCourse)} - Could not inserted - Return:: BadRequest");
                return BadRequest("Could not insert the course");
            }

            return RedirectToRoute(nameof(GetCourseById), new { id = course.Id });
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.PostCourse)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpPut("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> PutCourse(int id, [FromBody] PutCourseDto courseDto)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.PutCourse)} - Invalid model - Return:: BadRequest");
                return BadRequest("Invalid course");
            }

            await _courseService.EditAsync(id, courseDto);

            return await _saveChange.Handle<Course>(action: "Course edit successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.PutCourse)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpDelete("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> DeleteCourse(int id)
    {
        try
        {
            await _courseService.DeleteAsync(id);

            return await _saveChange.Handle<Course>(action: "Course delete successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.DeleteCourse)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpPost("AddStudent/{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> AddStudentAsync(int id, AddStudentCourseDto addStudentDto)
    {
        try
        {
            if (id <= 0)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.DeleteCourse)} - Invalid id - Return:: BadRequest");
                return BadRequest("Invalid id");
            }

            if (addStudentDto is null)
            {
                _logger.LogWarning($"{nameof(CourseController)}::{nameof(this.DeleteCourse)} - Invalid student - Return:: BadRequest");
                return BadRequest("Student id is required");
            }

            await _courseService.AddStudentAsync(id, addStudentDto);

            return await _saveChange.Handle<Course>(action: "Student add successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(CourseController)}::{nameof(this.AddStudentAsync)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion
}