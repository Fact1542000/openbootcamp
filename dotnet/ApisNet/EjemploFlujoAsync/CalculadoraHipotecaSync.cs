namespace EjemploFlujoAsync;

public static class CalculadoraHipotecaSync
{
    public static int ObtenerAniosVidaLaboral()
    {
        Console.WriteLine("\nObteniendo anos de vida laboral...");
        Task.Delay(5000).Wait();
        return new Random().Next(1, 35);
    }
    public static bool EsTipoContratoIndefinido()
    {
        Console.WriteLine("\nVerificando si el tipo de contrato es indefinido");
        Task.Delay(5000).Wait();
        return (new Random().Next(1, 10)) % 2 == 0;
    }
    public static int ObtenerSueldoNeto()
    {
        Console.WriteLine("\nObteniendo sueldo neto...");
        Task.Delay(5000).Wait();
        return new Random().Next(800, 6000);
    }
    public static int ObtenerGastosMensuales()
    {
        Console.WriteLine("\nObteniendo gastos mensuales del usuario...");
        Task.Delay(5000).Wait();
        return new Random().Next(200, 1000);
    }
    public static bool AnalizarInformacionParaCondecerHipoteca(
        int aniosVidaLaboral,
        bool tipoContradorEsIndefinido,
        int sueldoNeto,
        int gastosMensuales,
        int cantidadSolicitada,
        int aniosPagar
        )
    {
        System.Console.WriteLine("\nAnalizando informacion para conceder hipoteca...");
        if (aniosVidaLaboral < 2)
        {
            return false;
        }
        var couta = cantidadSolicitada / aniosPagar / 12;

        if (couta >= sueldoNeto || couta > (sueldoNeto / 2))
        {
            return false;
        }

        var porcentajeGastosSobreSueldo = ((gastosMensuales * 100) / sueldoNeto);

        if (porcentajeGastosSobreSueldo <= 30)
        {
            return false;
        }

        if ((couta + gastosMensuales) >= sueldoNeto)
        {
            return false;
        }

        if (!tipoContradorEsIndefinido)
        {
            return (couta + gastosMensuales) <= (sueldoNeto / 3);
        }

        return true;
    }
}