using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UniversityApi.Helpers;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Chapter;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Enums;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Controllers;
[Route("api/[controller]")]
[ApiController]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin)+","+nameof(Rol.User))]
public class ChapterController : ControllerBase
{
    private readonly SaveChangeHandle _saveChange;
    private readonly ILogger<ChapterController> _logger;
    private readonly IChapterService _chapterService;

    public ChapterController(SaveChangeHandle saveChange, IChapterService chapterService, ILogger<ChapterController> logger)
    {
        _saveChange = saveChange;
        _chapterService = chapterService;
        _logger = logger;
    }

    [HttpGet("GetAll")]
    public async Task<ActionResult<IEnumerable<GetChapterDto>>> GetAllChapters()
    {
        try
        {
            _logger.LogInformation($"{nameof(ChapterController)}::{nameof(this.GetAllChapters)} - Loading data...");
            var chapters = await _chapterService.GetAllAsync();
            return chapters.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(ChapterController)}::{nameof(this.GetAllChapters)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetById/{id}", Name = "GetChapterById")]
    public async Task<ActionResult<GetChapterDto>> GetChapterById(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(ChapterController)}::{nameof(this.GetChapterById)} - Loading data...");
            var chapter = await _chapterService.GetByIdAsync(id);
            if (chapter is null)
            {
                _logger.LogWarning($"{nameof(ChapterController)}::{nameof(this.GetChapterById)} - Return:: NotFound");
                NotFound($"Could not found the chapter with the id::{id}");
            }

            return chapter!;
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(ChapterController)}::{nameof(this.GetChapterById)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetCourse/{id}", Name = "GetCourseFromSpecificChapter")]
    public async Task<ActionResult<GetCourseDto>> GetCourseFromSpecificChapter(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(ChapterController)}::{nameof(this.GetCourseFromSpecificChapter)} - Loading data...");
            var course = await _chapterService.GetCourseFromSpecificChapter(id);
            if (course is null)
            {
                _logger.LogWarning($"{nameof(ChapterController)}::{nameof(this.GetCourseFromSpecificChapter)} - Return:: NotFound");
                NotFound("The chapter not have course related");
            }

            return course!;
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(ChapterController)}::{nameof(this.GetCourseFromSpecificChapter)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    #region WRITE
    [HttpPost]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> PostChapter([FromBody] PostChapterDto chapterDto)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(ChapterController)}::{nameof(this.PostChapter)} - Return:: NotFound");
                return BadRequest("Invalid chapter");
            }

            if (chapterDto.CourseId < 1)
            {
                _logger.LogWarning($"{nameof(ChapterController)}::{nameof(this.PostChapter)} - Return:: BadRequest");
                return BadRequest("Chapter need to be related with a course");
            }

            var chapter = await _chapterService.AddAsync(chapterDto);

            if (chapter is null)
            {
                _logger.LogWarning($"{nameof(ChapterController)}::{nameof(this.PostChapter)} - Return:: BadRequest");
                return BadRequest("Could not create the chapter");
            }

            return RedirectToRoute("GetChapterById", new { id = chapter.Id });
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(ChapterController)}::{nameof(this.PostChapter)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpPut("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> PutChapter(int id, [FromBody] PutChapterDto chapterDto)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(ChapterController)}::{nameof(this.PutChapter)} - Return:: BadRequest");
                return BadRequest("Invalid chapter");
            }

            await _chapterService.EditAsync(id, chapterDto);

            return await _saveChange.Handle<Chapter>(action: "Chapter edit successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(ChapterController)}::{nameof(this.PutChapter)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpDelete("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> DeleteChapter(int id)
    {
        try
        {
            await _chapterService.DeleteAsync(id);
            return await _saveChange.Handle<Chapter>(action: "Chapter delete successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(ChapterController)}::{nameof(this.DeleteChapter)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion
}