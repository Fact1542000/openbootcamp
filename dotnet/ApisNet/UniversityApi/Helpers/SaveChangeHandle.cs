using Microsoft.AspNetCore.Mvc;
using UniversityApi.AccessData;
using UniversityApi.Models.DataModels;

namespace UniversityApi.Helpers;
public class SaveChangeHandle : ControllerBase
{
    private readonly UniversityDbContext _db;

    public SaveChangeHandle(UniversityDbContext db)
    {
        _db = db;
    }

    public async Task<IActionResult> Handle<T>(string route = null!, T entity = null!, string action = null!) where T : BaseEntity
    {
        var result = await _db.SaveChangesAsync();
        if (result < 1) return BadRequest("Could not make the action");
        if (entity is not null && route is not null)
        {
            return RedirectToRoute(route, new { id = entity.Id });
        }
        if (action is not null)
        {
            return Ok(action);
        }
        return NoContent();
    }
}