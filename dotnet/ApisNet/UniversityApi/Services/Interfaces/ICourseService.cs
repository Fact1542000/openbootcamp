using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Dtos.Student;

namespace UniversityApi.Services.Interfaces;

public interface ICourseService
{
    #region READ
    Task<IEnumerable<GetCourseDto>> GetAllCourseWithSpecificCategory(int categoryId);
    Task<IEnumerable<GetCourseDto>> GetAllCourseWithoutSyllabus();
    Task<string> GetSyllabusFormSpecificCourse(int courseId);
    Task<IEnumerable<GetStudentDto>> GetStudentsFromSpecificCourse(int courseId);
    Task<IEnumerable<GetCourseDto>> GetAllCoursesWithStudents();
    Task<IEnumerable<GetCourseDto>> GetAllAsync();
    Task<GetCourseDto> GetByIdAsync(int id);
    Task<GetCourseDto> GetCourseWithCategory(int id);
    Task<GetCourseDto> GetCourseWithChapter(int id);
    Task<IEnumerable<GetCourseDto>> GetAllCourseWithAllField();
    #endregion

    #region Write
    Task<Course> AddAsync(PostCourseDto courseDto);
    Task AddStudentAsync(int courseId, AddStudentCourseDto addStudentCourseDto);
    Task EditAsync(int id, PutCourseDto courseDto);
    Task DeleteAsync(int id);
    Task<int> SaveChangeAsync();
    #endregion
}