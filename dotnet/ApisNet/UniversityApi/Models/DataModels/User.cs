using System.ComponentModel.DataAnnotations;
using UniversityApi.Models.Enums;

namespace UniversityApi.Models.DataModels;
public class User : BaseEntity
{
    public string Name { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    [EmailAddress]
    public string Email { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
    public Rol Rol { get; set; }
}