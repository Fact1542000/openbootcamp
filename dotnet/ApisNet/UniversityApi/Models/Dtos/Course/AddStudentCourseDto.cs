using System.ComponentModel.DataAnnotations;
namespace UniversityApi.Models.Dtos.Course;

public class AddStudentCourseDto
{
    [Required]
    public int StudentId { get; set; }
}