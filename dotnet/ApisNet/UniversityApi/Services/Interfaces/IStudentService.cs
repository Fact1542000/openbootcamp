using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Dtos.Student;

namespace UniversityApi.Services.Interfaces;

public interface IStudentService
{
    #region READ
    Task<IEnumerable<GetStudentDto>> GetAllWithoutEnrolledCourseAsync();
    Task<IEnumerable<GetCourseDto>> GetStudentCoursesAsync(int studentId);
    Task<IEnumerable<CourseStudent>> GetAllCourseStudentAsync();
    Task<IEnumerable<GetStudentDto>> GetAllStudentWithCoursesAsync();
    Task<IEnumerable<GetStudentDto>> GetAllAsync();
    Task<GetStudentDto> GetByIdAsync(int id);
    #endregion

    #region WRITE
    Task<Student> AddAsync(PostStudentDto studentDto);
    Task EditAsync(int id, PutStudentDto studentDto);
    Task DeleteAsync(int id);
    Task AddCourseAsync(int id, AddCourseStudentDto courseStudentDto);
    Task<int> SaveChangeAsync();
    #endregion
}