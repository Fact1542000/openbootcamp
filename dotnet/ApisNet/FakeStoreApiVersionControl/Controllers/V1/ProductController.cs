using System.Text.Json;
using FakeStoreApiVersionControl.Dtos.V1;
using Microsoft.AspNetCore.Mvc;

namespace FakeStoreApiVersionControl.Controllers.V1;

[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiController]
public class ProductController : ControllerBase
{
    private const string ApiUrl = "https://fakestoreapi.com/products";
    private readonly HttpClient _httpClient;
    public ProductController(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    [HttpGet(Name = "GetAllProductAsync")]
    [MapToApiVersion("1.0")]
    public async Task<IActionResult> GetAllProductAsync()
    {
        var response = await _httpClient.GetStreamAsync(ApiUrl);
        var productDto = await JsonSerializer.DeserializeAsync<Product[]>(response);
        return Ok(productDto);
    }
}