using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using UniversityApi;
using UniversityApi.AccessData;
using UniversityApi.Helpers;
using UniversityApi.Services;
using UniversityApi.Services.Interfaces;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
{
    // CONFIG SERILOG
    builder.Host.UseSerilog((hostBuilderCtx, loggerConf) =>
    {
        loggerConf
                .ReadFrom.Configuration(hostBuilderCtx.Configuration)
                .WriteTo.Console()
                .WriteTo.Debug();
    });

    // LOCALIZATION
    builder.Services.AddLocalization(opt => opt.ResourcesPath = "Resources");

    builder.Services.AddDbContext<UniversityDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("UniversityDb")));

    builder.Services.AddJwtTokenService(builder.Configuration);

    builder.Services.AddScoped<SaveChangeHandle>();

    builder.Services.AddScoped<IStudentService, StudentService>();
    builder.Services.AddScoped<ICourseService, CourseService>();
    builder.Services.AddScoped<IUserService, UserService>();
    builder.Services.AddScoped<IChapterService, ChapterService>();
    builder.Services.AddScoped<ICategoryService, CategoryService>();

    builder.Services.AddAutoMapper(typeof(MappingsProfile).Assembly);

    builder.Services.AddAuthorization(options => options.AddPolicy("UserOnlyPolicy", policy => policy.RequireClaim("UserOnly", "User1")));

    builder.Services.AddControllers();
    builder.Services.AddEndpointsApiExplorer();

    builder.Services.AddSwaggerGen(options => {
        // Define security
        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
        {
            Name = "Authorization",
            Type = SecuritySchemeType.Http,
            Scheme = "Bearer",
            BearerFormat = "JWT",
            In = ParameterLocation.Header,
            Description = "JWT Authorization Header using Bearer scheme"
        });
        options.AddSecurityRequirement(new OpenApiSecurityRequirement()
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference()
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                },
                Array.Empty<string>()
            }
        });
    });

    builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: "CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin();
                builder.AllowAnyMethod();
                builder.AllowAnyHeader();
            });
        });
}

var app = builder.Build();
{
    // SEEDING DATA
    using (var scope = app.Services.CreateScope())
    {
        var scopeProvider = scope.ServiceProvider;
        try
        {
            var _db = scopeProvider.GetRequiredService<UniversityDbContext>();
            await SeedData.SeedAsync(_db);
        }
        catch (Exception ex)
        {
            app.Logger.LogError(ex, "An error ocurred seeding the DB");
        }
    }

    // SUPPORTED CULTURES
    var supportedCultures = new[] { "en-US", "es-ES", "fr-FR", "DE" };
    var localizationOptions = new RequestLocalizationOptions()
                                    .SetDefaultCulture(supportedCultures[0])
                                    .AddSupportedCultures(supportedCultures)
                                    .AddSupportedUICultures(supportedCultures);

    app.UseRequestLocalization(localizationOptions);

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseSerilogRequestLogging();

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.UseCors("CorsPolicy");

    app.Run();
}