using System.Data;
using System.Reflection.Emit;
using LinqOB.Exercise.Models;

namespace LinqOB.Exercise;

public class Service
{
    private readonly List<Category> Categories = new();
    private readonly List<Course> Courses = new();
    private readonly List<User> Users = new();
    private readonly List<Student> Students = new();

    public User GetUserByEmail(string email)
    {
        var user = Users.Find(u => u.Email == email);
        return user!;
    }

    public List<Student> GetStudentWithLegalAge()
    {
        var studentsWithLegalAge = from student in Students
                                    where student.Age > 18
                                    select student;

        return studentsWithLegalAge.ToList();
    }

    public List<Student> GetStudentEnRolledInACourse()
    {
        var student = from std in Students
                        where std.Courses!.Count > 0 || std.Courses != null
                        select std;

        return student.ToList();
    }

    public List<Course> GetCourseWithSpecificLevenAndStudentEnrolled(Level level)
    {
        var courses = from course in Courses
                        where course.Level == level && course.Students != null
                        select course;

        return courses.ToList();
    }

    public List<Course> GetCourseWithSpecificLevenAndSpecificCategory(Level level, string category)
    {
        var courses = from course in Courses
                        where course.Level == level && course.Category!.Any(c => c.Name == category)
                        select course;

        return courses.ToList();
    }

    public List<Course> GetCourseWithoutStudentEnrolled()
    {
        var courses = from course in Courses
                        where course.Students == null || course.Students!.Count < 1
                        select course;
        return courses.ToList();
    }

    public void SeedData()
    {
        var categoryProgramming = new Category
        {
            Id = 1,
            Name = "Programming",
        };
        var categoryEconomy = new Category
        {
            Id = 2,
            Name = "Economy",
        };
        var categoryWebDevelopment = new Category
        {
            Id = 3,
            Name = "Web development"
        };
        Categories.Add(categoryEconomy);
        Categories.Add(categoryProgramming);
        Categories.Add(categoryWebDevelopment);

        var user1 = new User
        {
            Id = 1,
            Name = "Francis",
            Email = "Francis@francis.com",
            Password = "francis.123",
        };
        var user2 = new User
        {
            Id = 2,
            Name = "Emnny",
            Email = "Emnny@emnny.com",
            Password = "emnny.123",
        };
        var user3 = new User
        {
            Id = 3,
            Name = "Jennifer",
            Email = "Jennifer@emnny.com",
            Password = "jennifer.123",
        };
        Users.Add(user1);
        Users.Add(user2);
        Users.Add(user3);

        var student1 = new Student
        {
            Id = 1,
            Name = "Pablo",
            LastName = "Perez",
            Age = 17,
            Courses = Courses
        };
        var student2 = new Student
        {
            Id = 2,
            Name = "Paula",
            LastName = "Gonzales",
            Age = 20,
            Courses = Courses
        };
        var student3 = new Student
        {
            Id = 3,
            Name = "Cesar",
            LastName = "Pujols",
            Age = 25,
            Courses = Courses
        };
        Students.Add(student1);
        Students.Add(student2);
        Students.Add(student3);

        var course1 = new Course
        {
            Id = 1,
            Name = "Golang",
            Level = Level.Medium,
            Category = new() {categoryProgramming},
            Students = Students
        };
        var course2 = new Course
        {
            Id = 2,
            Name = "React",
            Level = Level.Entry,
            Category = new() {categoryWebDevelopment},
            Students = Students
        };
        var course3 = new Course
        {
            Id = 3,
            Name = "Economy",
            Level = Level.Advanced,
            Category = new() {categoryEconomy},
            Students = Students
        };
        var course4 = new Course
        {
            Id = 4,
            Name = "Other",
            Level = Level.Advanced,
            Category = new() {categoryEconomy},
        };
        Courses.Add(course1);
        Courses.Add(course2);
        Courses.Add(course3);
        Courses.Add(course4);
    }
}