using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using ApiVersionControl;

var builder = WebApplication.CreateBuilder(args);
{
    // 1. Add HttpClient to send HttpRequest in controllers
    builder.Services.AddHttpClient();

    // 2. Add App Versioning Control
    builder.Services.AddApiVersioning(setup => {
        setup.DefaultApiVersion = new ApiVersion(1, 0);
        setup.AssumeDefaultVersionWhenUnspecified = true;
        setup.ReportApiVersions = true;
    });

    // 3. Add Configuration to document versions
    builder.Services.AddVersionedApiExplorer(setup => {
        setup.GroupNameFormat = "'v'VVV";
        setup.SubstituteApiVersionInUrl = true;
    });

    builder.Services.AddControllers();
    // builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    // 4. Configure options
    builder.Services.ConfigureOptions<SwaggerOptionsConfigure>();
}

var app = builder.Build();
{
    // 5. Configure Endpoints for swagger DOCS for each
    var apiVersionDescriptionProvider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        // 6. Configure swagger DOCS
        app.UseSwaggerUI(options => {
            foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions)
            {
                options.SwaggerEndpoint(
                    $"{description.GroupName}/swagger.json",
                    description.GroupName.ToUpperInvariant()
                );
            }
        });
    }

    app.UseHttpsRedirection();
    app.MapControllers();
    app.Run();
}