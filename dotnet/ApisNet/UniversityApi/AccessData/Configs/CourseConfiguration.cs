using Microsoft.EntityFrameworkCore;
using UniversityApi.Models.DataModels;

namespace UniversityApi.AccessData.Configs;

public class CourseConfiguration : IEntityTypeConfiguration<Course>
{
    public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Course> builder)
    {
        builder.HasKey(c => c.Id);
        builder.Property(c => c.ShortDescription).HasMaxLength(280);
    }
}