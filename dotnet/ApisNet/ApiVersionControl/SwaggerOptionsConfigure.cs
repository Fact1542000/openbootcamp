using System.Security;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ApiVersionControl;

public class SwaggerOptionsConfigure : IConfigureNamedOptions<SwaggerGenOptions>
{
    private readonly IApiVersionDescriptionProvider _provider;

    public SwaggerOptionsConfigure(IApiVersionDescriptionProvider provider)
    {
        _provider = provider;
    }

    public void Configure(string name, SwaggerGenOptions options)
    {
        Configure(options);
    }

    public void Configure(SwaggerGenOptions options)
    {
        // Add Swagger Documentation for each API version we have
        foreach (var description in _provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(description.GroupName, CreateVersionInfo(description));
        }
    }

    private static OpenApiInfo CreateVersionInfo(ApiVersionDescription description)
    {
        var info = new OpenApiInfo()
        {
            Title = "My .Net API restful",
            Version = description.ApiVersion.ToString(),
            Description = "This is my first API Versioning control",
            Contact = new OpenApiContact()
            {
                Email = "francisalcantara001@gmail.com",
                Name = "Francis"
            }
        };

        if (description.IsDeprecated)
        {
            info.Description += "This API Version has been depretcated";
        }
        return info;
    }
}