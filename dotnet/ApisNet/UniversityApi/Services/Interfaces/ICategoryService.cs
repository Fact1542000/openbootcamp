using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Category;
using UniversityApi.Models.Dtos.Course;

namespace UniversityApi.Services.Interfaces;

public interface ICategoryService
{
    #region READ
    Task<IEnumerable<GetCategoryDto>> GetAllAsync();
    Task<GetCategoryDto> GetByIdAsync(int id);
    Task<IEnumerable<GetCourseDto>> GetAllCourseRelatedWithCategory(int id);
    #endregion

    #region WRITE
    Task<Category> AddAsync(PostCategoryDto categoryDto);
    Task DeleteAsync(int id);
    Task EditAsync(int id, PutCategoryDto categoryDto);
    Task AddCourse(int id, AddCategoryCourseDto courseDto);
    Task<int> SaveChangeAsync();
    #endregion
}