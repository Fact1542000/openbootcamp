﻿using EjemploFlujoAsync;
using System.Diagnostics;

// Iniciamos un contador de tiempo - SINCRONO
Stopwatch stopwatch = new Stopwatch();
stopwatch.Start();

Console.WriteLine("\n***************************************************");
Console.WriteLine("\nBienvenido a la calculadora de hipotecas sincrona");
Console.WriteLine("\n***************************************************");

var aniosVidaLaboral = CalculadoraHipotecaSync.ObtenerAniosVidaLaboral();
Console.WriteLine($"\nAnos de vida laboral obtenidos: {aniosVidaLaboral}");

var esTipoContratoIndefinido = CalculadoraHipotecaSync.EsTipoContratoIndefinido();
Console.WriteLine($"Tipo de contrato indefinido: { esTipoContratoIndefinido }");

var sueldoNeto = CalculadoraHipotecaSync.ObtenerSueldoNeto();
Console.WriteLine($"Sueldo neto obteneido: { sueldoNeto }");

var gastosMensuales = CalculadoraHipotecaSync.ObtenerGastosMensuales();
Console.WriteLine($"Gastos mensuales obtenidos: { gastosMensuales }");

var hipotecaConcedida = CalculadoraHipotecaSync.AnalizarInformacionParaCondecerHipoteca( 
    aniosVidaLaboral: aniosVidaLaboral,
    tipoContradorEsIndefinido: esTipoContratoIndefinido,
    sueldoNeto: sueldoNeto,
    gastosMensuales: gastosMensuales,
    cantidadSolicitada: 5000,
    aniosPagar: 30
);

var resultado = hipotecaConcedida ? "APROBADA" : "DENEGADA";

Console.WriteLine($"\nAnalisis finalizado. su solicitud de hipoteca ha sido: {resultado}");

stopwatch.Stop();

Console.WriteLine($"\nLa operacion ha durado: {stopwatch.Elapsed}");

// Reiniciamos un contador de tiempo - ASINCRONO

stopwatch.Restart();

stopwatch.Start();

Console.WriteLine("\n*****************************************************");
Console.WriteLine("\nBienvenido a la calculadora de hipotecas asincrona");
Console.WriteLine("\n*****************************************************");

var aniosVidaLaboralAync = CalculadoraHipotecaAsync.ObtenerAniosVidaLaboral();

var esTipoContratoIndefinidoAsync = CalculadoraHipotecaAsync.EsTipoContratoIndefinido();

var sueldoNetoAsync = CalculadoraHipotecaAsync.ObtenerSueldoNeto();

var gastosMensualesAsync = CalculadoraHipotecaAsync.ObtenerGastosMensuales();

var analisisHipotecaTasks = new List<Task>
{
    aniosVidaLaboralAync,
    esTipoContratoIndefinidoAsync,
    sueldoNetoAsync,
    gastosMensualesAsync
};

while (analisisHipotecaTasks.Any())
{
    Task tareaFinalizada = await Task.WhenAny(analisisHipotecaTasks);
    if (tareaFinalizada == aniosVidaLaboralAync)
    {
        Console.WriteLine($"\nAnos de vida laboral obtenidos: {aniosVidaLaboralAync.Result}");
    }
    else if (tareaFinalizada == esTipoContratoIndefinidoAsync)
    {
        Console.WriteLine($"Tipo de contrato indefinido: { esTipoContratoIndefinidoAsync.Result }");
    }
    else if (tareaFinalizada == sueldoNetoAsync)
    {
        Console.WriteLine($"Sueldo neto obteneido: { sueldoNetoAsync }");
    }
    else if (tareaFinalizada == gastosMensualesAsync)
    {
        Console.WriteLine($"Gastos mensuales obtenidos: { gastosMensualesAsync }");
    }
    analisisHipotecaTasks.Remove(tareaFinalizada);
}

var hipotecaConcedidaAsync = CalculadoraHipotecaAsync.AnalizarInformacionParaCondecerHipoteca( 
    aniosVidaLaboral: aniosVidaLaboralAync.Result,
    tipoContradorEsIndefinido: esTipoContratoIndefinidoAsync.Result,
    sueldoNeto: sueldoNetoAsync.Result,
    gastosMensuales: gastosMensualesAsync.Result,
    cantidadSolicitada: 5000,
    aniosPagar: 30
);

var resultadoAsync = hipotecaConcedidaAsync ? "APROBADA" : "DENEGADA";

Console.WriteLine($"\nAnalisis finalizado. su solicitud de hipoteca ha sido: {resultadoAsync}");

stopwatch.Stop();

Console.WriteLine($"\nLa operacion ha durado: {stopwatch.Elapsed}");

Console.Read();