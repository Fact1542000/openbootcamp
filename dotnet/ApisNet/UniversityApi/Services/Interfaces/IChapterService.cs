using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Chapter;
using UniversityApi.Models.Dtos.Course;

namespace UniversityApi.Services.Interfaces;

public interface IChapterService
{
    #region READ
    Task<IEnumerable<GetChapterDto>> GetAllAsync();
    Task<GetChapterDto> GetByIdAsync(int id);
    Task<GetCourseDto> GetCourseFromSpecificChapter(int chapterId);
    #endregion

    #region WRITE
    Task<Chapter> AddAsync(PostChapterDto chapterDto);
    Task EditAsync(int id, PutChapterDto chapterDto);
    Task DeleteAsync(int id);
    Task<int> SaveChangeAsync();
    #endregion
}