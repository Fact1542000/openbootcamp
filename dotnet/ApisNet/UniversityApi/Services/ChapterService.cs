using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UniversityApi.AccessData;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Chapter;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Services;

public class ChapterService : IChapterService
{
    private readonly UniversityDbContext _db;
    private readonly IMapper _mapper;
    private readonly ILogger<ChapterService> _logger;

    public ChapterService(UniversityDbContext db, IMapper mapper, ILogger<ChapterService> logger)
    {
        _db = db;
        _mapper = mapper;
        _logger = logger;
    }

    #region READ
    public async Task<IEnumerable<GetChapterDto>> GetAllAsync()
    {
        _logger.LogInformation($"{nameof(ChapterService)}::{nameof(this.GetAllAsync)} - Called");
        return await _db.Chapters.Select(c => _mapper.Map<GetChapterDto>(c)).ToListAsync();
    }

    public async Task<GetChapterDto> GetByIdAsync(int id)
    {
        _logger.LogInformation($"{nameof(ChapterService)}::{nameof(this.GetByIdAsync)} - Called");
        var chapter = await _db.Chapters.FirstOrDefaultAsync(c => c.Id == id);
        return _mapper.Map<GetChapterDto>(chapter);
    }

    public async Task<GetCourseDto> GetCourseFromSpecificChapter(int chapterId)
    {
        _logger.LogInformation($"{nameof(ChapterService)}::{nameof(this.GetCourseFromSpecificChapter)} - Called");
        var chapter = await _db.Chapters.FindAsync(chapterId);
        var course = await _db.Courses.FindAsync(chapter!.CourseId);
        var courseDto = _mapper.Map<GetCourseDto>(course);
        courseDto.Chapter = chapter.List;
        return courseDto;
    }
    #endregion

    #region WRITE
    public async Task<Chapter> AddAsync(PostChapterDto chapterDto)
    {
        _logger.LogInformation($"{nameof(ChapterService)}::{nameof(this.AddAsync)} - Called");
        var chapter = _mapper.Map<Chapter>(chapterDto);
        await _db.Chapters.AddAsync(chapter);
        await SaveChangeAsync();
        return chapter;
    }

    public async Task DeleteAsync(int id)
    {
        _logger.LogInformation($"{nameof(ChapterService)}::{nameof(this.DeleteAsync)} - Called");
        var chapter = await _db.Chapters.FindAsync(id);
        _db.Chapters.Remove(chapter!);
    }

    public async Task EditAsync(int id, PutChapterDto chapterDto)
    {
        _logger.LogInformation($"{nameof(ChapterService)}::{nameof(this.EditAsync)} - Called");
        var chapter = await _db.Chapters.FindAsync(id);
        if (chapterDto.CourseId < 1 && chapter!.CourseId > 0)
        {
            chapterDto.CourseId = chapter!.CourseId;
        }
        _mapper.Map(chapterDto, chapter);
    }

    public async Task<int> SaveChangeAsync()
    {
        return await _db.SaveChangesAsync();
    }
    #endregion
}