namespace EjemploFlujoAsync;
public static class CalculadoraHipotecaAsync
{
    public async static Task<int> ObtenerAniosVidaLaboral()
    {
        Console.WriteLine("\nObteniendo anos de vida laboral...");
        await Task.Delay(5000);
        return new Random().Next(1, 35);
    }
    public async static Task<bool> EsTipoContratoIndefinido()
    {
        Console.WriteLine("\nVerificando si el tipo de contrato es indefinido");
        await Task.Delay(5000);
        return (new Random().Next(1, 10)) % 2 == 0;
    }
    public async static Task<int> ObtenerSueldoNeto()
    {
        Console.WriteLine("\nObteniendo sueldo neto...");
        await Task.Delay(5000);
        return new Random().Next(800, 6000);
    }
    public async static Task<int> ObtenerGastosMensuales()
    {
        Console.WriteLine("\nObteniendo gastos mensuales del usuario...");
        await Task.Delay(5000);
        return new Random().Next(200, 1000);
    }
    public static bool AnalizarInformacionParaCondecerHipoteca(
        int aniosVidaLaboral,
        bool tipoContradorEsIndefinido,
        int sueldoNeto,
        int gastosMensuales,
        int cantidadSolicitada,
        int aniosPagar
        )
    {
        System.Console.WriteLine("\nAnalizando informacion para conceder hipoteca...");
        if (aniosVidaLaboral < 2)
        {
            return false;
        }
        var couta = cantidadSolicitada / aniosPagar / 12;

        if (couta >= sueldoNeto || couta > (sueldoNeto / 2))
        {
            return false;
        }

        var porcentajeGastosSobreSueldo = ((gastosMensuales * 100) / sueldoNeto);

        if (porcentajeGastosSobreSueldo <= 30)
        {
            return false;
        }

        if ((couta + gastosMensuales) >= sueldoNeto)
        {
            return false;
        }

        if (!tipoContradorEsIndefinido)
        {
            return (couta + gastosMensuales) <= (sueldoNeto / 3);
        }

        return true;
    }
}