namespace UniversityApi.Models.Enums;

public enum Rol
{
    Admin,
    User
}