﻿using LinqOB.Exercise;
using LinqOB.Exercise.Models;
using LinqOB.LinqClass;

var service = new Service();
service.SeedData();

// 1. Users by email
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("User by email");
Console.ForegroundColor = ConsoleColor.Blue;
var userByEmail = service.GetUserByEmail("Francis@francis.com");
Console.WriteLine($"Name::{userByEmail.Name}\nEmail::{userByEmail.Email}\n");
Console.ResetColor();

// 2. Student of legal age
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Student of legal age");
Console.ForegroundColor = ConsoleColor.Blue;
var studentsOfLegalAge = service.GetStudentWithLegalAge();
studentsOfLegalAge.ForEach((student) => Console.WriteLine($"Name::{student.Name}\nLastName::{student.LastName}\nAge::{student.Age}\nRolledCourses::{student.Courses!.Count}\n"));
Console.ResetColor();

// 3. Student enrolled in a course
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Student enrolled in a course");
Console.ForegroundColor = ConsoleColor.Blue;
var studentsEnrolledInACourse = service.GetStudentEnRolledInACourse();
studentsEnrolledInACourse.ForEach((student) => Console.WriteLine($"Name::{student.Name}\nLastName::{student.LastName}\nAge::{student.Age}\nRolledCourses::{student.Courses!.Count}\n"));
Console.ResetColor();

// 4. Course with specific level and student enrolled 
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Course with specific level and student enrolled");
Console.ForegroundColor = ConsoleColor.Blue;
var courseWithSpecificLevelAndStudenEnrolled = service.GetCourseWithSpecificLevenAndStudentEnrolled(level: Level.Advanced);
courseWithSpecificLevelAndStudenEnrolled.ForEach((course) => Console.WriteLine($"Name::{course.Name}\nLevel::{course.Level}\nNumberOfStudents::{course.Students!.Count}\n"));
Console.ResetColor();

// 5. Course with specific level and specific category
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Course with specific level and specific category");
Console.ForegroundColor = ConsoleColor.Blue;
var courseWithSpecificLevelAndSpecificCategory = service.GetCourseWithSpecificLevenAndSpecificCategory(level: Level.Medium, category: "Programming");
courseWithSpecificLevelAndSpecificCategory.ForEach((course) => Console.WriteLine($"Name::{course.Name}\nLevel::{course.Level}\nNumberOfStudents::{course.Students!.Count}\n"));
Console.ResetColor();

// 6. Course without student enrolled
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Course without student entolled");
Console.ForegroundColor = ConsoleColor.Blue;
var courseWithoutStudentEnrolled = service.GetCourseWithoutStudentEnrolled();
courseWithoutStudentEnrolled.ForEach((course) => Console.WriteLine($"Name::{course.Name}\nLevel::{course.Level}\n"));
Console.ResetColor();

Snippets.GroupByExample();