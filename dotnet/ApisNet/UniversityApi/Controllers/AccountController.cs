using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using UniversityApi.Models.Dtos.User;
using UniversityApi.Models.Enums;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class AccountController : ControllerBase
{
    private readonly ILogger<AccountController> _logger;
    private readonly IUserService _userService;
    private readonly IStringLocalizer<AccountController> _stringLocalizer;

    public AccountController(IUserService userService, IStringLocalizer<AccountController> stringLocalizer, ILogger<AccountController> logger)
    {
        _userService = userService;
        _stringLocalizer = stringLocalizer;
        _logger = logger;
    }

    [HttpPost]
    public async Task<IActionResult> GetToken([FromBody] UserLoginDto userDto)
    {
        try
        {
            _logger.LogInformation($"{nameof(AccountController)}::{nameof(GetToken)} - Login a user...");
            var userToken = await _userService.LoginAsync(userDto);

            if (userToken is null) return BadRequest("could not login, check your email or password and try again");
            _logger.LogInformation($"{nameof(AccountController)}::{nameof(GetToken)} - User logged in");
            return Ok(new
            {
                Token = userToken,
                Message = String.Format(_stringLocalizer.GetString("LoginMessage"), userToken.UserName)
            });
        }
        catch(Exception ex)
        {
            _logger.LogError($"{nameof(AccountController)}::{nameof(GetToken)} - Exception:: {ex.Message}");
            throw new Exception("GetToken error", ex);
        }
    }

    [HttpGet]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> GetUserList()
    {
        try
        {
            _logger.LogInformation($"{nameof(AccountController)}::{nameof(GetUserList)} - Loading data...");
            var users = await _userService.GetAllAsync();
            return Ok(users);
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(AccountController)}::{nameof(GetToken)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
}