using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UniversityApi.Helpers;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.User;
using UniversityApi.Models.Enums;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Controllers;
[Route("api/[controller]")]
[ApiController]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
public class UserController : ControllerBase
{
    private readonly IUserService _userService;
    private readonly ILogger<UserController> _logger;
    private readonly SaveChangeHandle _saveChange;

    public UserController(SaveChangeHandle saveChange, IUserService userService, ILogger<UserController> logger)
    {
        _saveChange = saveChange;
        _userService = userService;
        _logger = logger;
    }

    #region READ
    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetUserDto>>> GetAllUsers()
    {
        try
        {
            _logger.LogInformation($"{nameof(UserController)}::{nameof(this.GetAllUsers)} - Loading data...");

            var users = await _userService.GetAllAsync();

            return users.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(UserController)}::{nameof(this.GetAllUsers)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("{id}", Name = "GetById")]
    public async Task<ActionResult<GetUserDto>> GetUserById(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(UserController)}::{nameof(this.GetUserById)} - Loading data...");

            if (id <= 0)
            {
                _logger.LogWarning($"{nameof(UserController)}::{nameof(this.GetUserById)} - Invalid id - Return:: BadRequest");
                return BadRequest("Invalid id");
            }

            var user = await _userService.GetByIdAsync(id);

            if (user is null)
            {
                _logger.LogWarning($"{nameof(UserController)}::{nameof(this.GetUserById)} - Return:: NotFound");
                return NotFound($"Could not found the user with the id::{id}");
            }

            return user;
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(UserController)}::{nameof(this.GetUserById)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region WRITE
    [HttpPost]
    public async Task<IActionResult> PostUser([FromBody] PostUserDto userDto)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(UserController)}::{nameof(this.PostUser)} - Invalid model - Return:: BadRequest");
                return BadRequest("Invalid user");
            }

            var user = await _userService.AddAsync(userDto);

            if (user is null)
            {
                _logger.LogWarning($"{nameof(UserController)}::{nameof(this.PostUser)} - Could not inserted - Return:: BadRequest");
                return BadRequest("Could not insert the user");
            }

            return RedirectToRoute(nameof(GetUserById), new { user.Id });
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(UserController)}::{nameof(this.PostUser)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutUser(int id, [FromBody] PutUserDto userDto)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(UserController)}::{nameof(this.PutUser)} - Invalid model - Return:: BadRequest");
                return BadRequest("Invalid user");
            }

            await _userService.EditAsync(id, userDto);

            return await _saveChange.Handle<User>(action: "User edit successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(UserController)}::{nameof(this.PutUser)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteUser(int id)
    {
        try
        {
            if (id < 1)
            {
                _logger.LogWarning($"{nameof(UserController)}::{nameof(this.DeleteUser)} - Invalid id - Return:: BadRequest");
                return BadRequest("Invalid id");
            }

            await _userService.DeleteAsync(id);

            return await _saveChange.Handle<User>(action: "User remove successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(UserController)}::{nameof(this.DeleteUser)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion
}