namespace UniversityApi.Models.Dtos.User;

public class GetUserDto
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Rol { get; set; } = null!;
}