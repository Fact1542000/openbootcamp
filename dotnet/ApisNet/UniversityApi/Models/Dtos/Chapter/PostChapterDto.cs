namespace UniversityApi.Models.Dtos.Chapter;

public class PostChapterDto
{
    public int CourseId { get; set; }
    public string List { get; set; } = null!;
    public DateTime CreatedAt { get; set; }
}