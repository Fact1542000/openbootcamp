using UniversityApi.Models.Dtos.Course;

namespace UniversityApi.Models.Dtos.Category;

public class GetCategoryDto
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public List<GetCourseDto> Courses { get; set; } = new();
}