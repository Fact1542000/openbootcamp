using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UniversityApi.AccessData;
using UniversityApi.Helpers;
using UniversityApi.Models;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.User;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Services;

public class UserService : IUserService
{
    private readonly UniversityDbContext _db;
    private readonly JwtSettings _jwtSettings;
    private readonly IMapper _mapper;
    private readonly ILogger<UserService> _logger;

    public UserService(UniversityDbContext db, IMapper mapper, JwtSettings jwtSettings, ILogger<UserService> logger)
    {
        _db = db;
        _mapper = mapper;
        _jwtSettings = jwtSettings;
        _logger = logger;
    }

    #region READ
    public async Task<IEnumerable<GetUserDto>> GetAllAsync()
    {
        _logger.LogInformation($"{nameof(UserService)}::{nameof(this.GetAllAsync)} - Called");
        return await _db.Users.Select(u => _mapper.Map<GetUserDto>(u)).ToListAsync();
    }

    public async Task<GetUserDto> GetByIdAsync(int id)
    {
        _logger.LogInformation($"{nameof(UserService)}::{nameof(this.GetByIdAsync)} - Called");
        var user = await _db.Users.FindAsync(id);
        return _mapper.Map<GetUserDto>(user);
    }
    #endregion

    #region WRITE
    public async Task<User> AddAsync(PostUserDto userDto)
    {
        _logger.LogInformation($"{nameof(UserService)}::{nameof(this.AddAsync)} - Called");
        var user = _mapper.Map<User>(userDto);
        await _db.Users.AddAsync(user);
        await SaveChangeAsync();
        return user;
    }

    public async Task DeleteAsync(int id)
    {
        _logger.LogInformation($"{nameof(UserService)}::{nameof(this.DeleteAsync)} - Called");
        var user = await _db.Users.FindAsync(id);
        _db.Users.Remove(user!);
    }

    public async Task EditAsync(int id, PutUserDto userDto)
    {
        _logger.LogInformation($"{nameof(UserService)}::{nameof(this.EditAsync)} - Called");
        var user = await _db.Users.FindAsync(id);
        _mapper.Map(userDto, user);
    }

    public async Task<UserTokens> LoginAsync(UserLoginDto userLoginDto)
    {
        _logger.LogInformation($"{nameof(UserService)}::{nameof(this.LoginAsync)} - Called");
        var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == userLoginDto.Email && u.Password == userLoginDto.Password);
        var userToken = new UserTokens();
        if (user is not null)
        {
            userToken = JwtHelpers.GenerateTokenKey(new UserTokens()
            {
                UserName = user.Name,
                EmailId = user.Email,
                Id = user.Id,
                GuidId = Guid.NewGuid()
            }, _jwtSettings);
        }
        return userToken;
    }

    public async Task<int> SaveChangeAsync()
    {
        return await _db.SaveChangesAsync();
    }
    #endregion
}