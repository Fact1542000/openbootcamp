using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UniversityApi.Helpers;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Dtos.Student;
using UniversityApi.Models.Enums;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.User) + "," + nameof(Rol.Admin))]
public class StudentController : ControllerBase
{
    private readonly ILogger<StudentController> _logger;
    private readonly SaveChangeHandle _saveChange;
    private readonly IStudentService _studentService;

    public StudentController(SaveChangeHandle saveChange, IStudentService studentService, ILogger<StudentController> logger)
    {
        _saveChange = saveChange;
        _studentService = studentService;
        _logger = logger;
    }

    #region READ
    [HttpGet("withoutEnrolledCourse")]
    public async Task<ActionResult<IEnumerable<GetStudentDto>>> GetAllStudentWithoutEnrolledCourse()
    {
        try
        {
            _logger.LogInformation($"{nameof(StudentController)}::{nameof(this.GetAllStudentWithoutEnrolledCourse)} - Loading data...");

            var students = await _studentService.GetAllWithoutEnrolledCourseAsync();

            if (students is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.GetAllStudentWithoutEnrolledCourse)} - Return:: NotFound");
                NotFound("Could not found any student without enrolled course");
            }

            return students!.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.GetAllStudentWithoutEnrolledCourse)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("{id}/courses", Name = "GetCoursesFromSpecificStudent")]
    public async Task<ActionResult<IEnumerable<GetCourseDto>>> GetCoursesFromSpecificStudent(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(StudentController)}::{nameof(this.GetCoursesFromSpecificStudent)} - Loading data...");

            var courses = await _studentService.GetStudentCoursesAsync(id);

            if (courses is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.GetCoursesFromSpecificStudent)} - Return:: NotFound");
                return NotFound($"Could not found a user with the id::{id}");
            }

            return courses.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.GetCoursesFromSpecificStudent)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetAllStudents")]
    public async Task<ActionResult<IEnumerable<GetStudentDto>>> GetAllStudents()
    {
        try
        {
            _logger.LogInformation($"{nameof(StudentController)}::{nameof(this.GetAllStudents)} - Loading data...");

            var students = await _studentService.GetAllAsync();

            return students.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.GetAllStudents)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("GetAllStudentsWithEnrolledCourses")]
    public async Task<ActionResult<IEnumerable<GetStudentDto>>> GetStudentsWithCourses()
    {
        try
        {
            _logger.LogInformation($"{nameof(StudentController)}::{nameof(this.GetStudentsWithCourses)} - Loading data...");

            var studentWithCourses = await _studentService.GetAllStudentWithCoursesAsync();

            return studentWithCourses.ToList();
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.GetStudentsWithCourses)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpGet("{id}", Name = "GetStudentById")]
    public async Task<ActionResult<GetStudentDto>> GetStudentById(int id)
    {
        try
        {
            _logger.LogInformation($"{nameof(StudentController)}::{nameof(this.GetStudentById)} - Loading data...");

            var student = await _studentService.GetByIdAsync(id);

            if (student is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.GetStudentById)} - Return:: NotFound");
                NotFound($"Could not found the student with the id::{id}");
            }

            return student!;
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.GetStudentById)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region WRITE
    [HttpPost]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> PostStudent([FromBody] PostStudentDto postStudentDto)
    {
        try
        {
            if (postStudentDto is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.PostStudent)} - Invalid student - Return:: BadRequest");
                return BadRequest("Invalid student");
            }

            var student = await _studentService.AddAsync(postStudentDto);

            if (student is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.PostStudent)} - Could not inserted - Return:: BadRequest");
                return BadRequest("Could not insert the student");
            }

            return await _saveChange.Handle<Student>("GetStudentById", student);
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.PostStudent)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpPost("AddCourse/{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> AddCourseToAStudent(int id, [FromBody] AddCourseStudentDto addCourseDto)
    {
        try
        {
            var student = await _studentService.GetByIdAsync(id);

            if (student is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.AddCourseToAStudent)} - Could not inserted - Return:: NotFound");
                NotFound($"Could not found a student with the id::{id}");
            }

            await _studentService.AddCourseAsync(id, addCourseDto);

            return await _saveChange.Handle<Student>(action: "CourseStudent added successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.AddCourseToAStudent)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpPut("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> PutStudent(int id, [FromBody] PutStudentDto putStudentDto)
    {
        try
        {
            var studentDb = await _studentService.GetByIdAsync(id);

            if (studentDb is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.PutStudent)} - Return:: NotFound");
                return NotFound($"Could not found a student with the id::{id}");
            }

            if (putStudentDto is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.PutStudent)} - Return:: BadRequest");
                return BadRequest("Invalid student");
            }

            await _studentService.EditAsync(id, putStudentDto);

            return await _saveChange.Handle<Student>(action: "Student edit successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.PutStudent)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }

    [HttpDelete("{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = nameof(Rol.Admin))]
    public async Task<IActionResult> DeleteStudent(int id)
    {
        try
        {
            var student = await _studentService.GetByIdAsync(id);

            if (student is null)
            {
                _logger.LogWarning($"{nameof(StudentController)}::{nameof(this.DeleteStudent)} - Return:: NotFound");
                return NotFound($"Could not found a user with the id {id}");
            }

            await _studentService.DeleteAsync(id);

            return await _saveChange.Handle<Student>(action: "Student deleted successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError($"{nameof(StudentController)}::{nameof(this.DeleteStudent)} - Exception:: {ex.Message}");
            throw new Exception(ex.Message);
        }
    }
    #endregion
}