using System.ComponentModel.DataAnnotations;
using UniversityApi.Models.Enums;

namespace UniversityApi.Models.Dtos.Course;

public class GetCourseDto
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string Description { get; set; } = null!;
    public string ObjetiveAudience { get; set; } = null!;
    public string Goals { get; set; } = null!;
    public string Requirements { get; set; } = null!;
    public string Syllabus { get; set; } = null!;
    public Level Level { get; set; }
    public List<string> Students { get; set; } = new();
    public List<string> Categories { get; set; } = new();
    public string Chapter { get; set; } = string.Empty;
}