namespace UniversityApi.Models.Dtos.Chapter;

public class GetChapterDto
{
    public int Id { get; set; }
    public string List { get; set; } = null!;
}