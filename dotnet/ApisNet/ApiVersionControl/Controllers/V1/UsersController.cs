using System.Text.Json;
using ApiVersionControl.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace ApiVersionControl.Controllers.V1;

[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    private const string ApiTestUrl = "https://dummyapi.io/data/v1/user?limit=30";
    private const string AppId = "62efe2278d88f79eba739681";
    private readonly HttpClient _httpClient;

    public UsersController(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    [HttpGet(Name = "GetUserData")]
    [MapToApiVersion("1.0")]
    public async Task<IActionResult> GetUserDataAsync()
    {
        _httpClient.DefaultRequestHeaders.Clear();
        _httpClient.DefaultRequestHeaders.Add("app-id", AppId);

        var response = await _httpClient.GetStreamAsync(ApiTestUrl);

        var usersData = await JsonSerializer.DeserializeAsync<UsersResponseData>(response);

        return Ok(usersData);
    }
}
