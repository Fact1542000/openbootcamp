using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using UniversityApi.Models.DataModels;

namespace UniversityApi.AccessData;

public static class SeedData
{
    public static async Task SeedAsync(UniversityDbContext _context)
    {
        if (!await _context.Courses.AnyAsync())
        {
            await _context.Courses.AddRangeAsync(GetPreconfiguredCourses());
            await _context.SaveChangesAsync();
        }
        if (!await _context.Students.AnyAsync())
        {
            await _context.Students.AddRangeAsync(GetPreconfiguredStudents());
            await _context.SaveChangesAsync();
        }
        if (!await _context.Chapters.AnyAsync())
        {
            await _context.Chapters.AddRangeAsync(GetPreconfiguredChapters());
            await _context.SaveChangesAsync();
        }
        if (!await _context.Categories.AnyAsync())
        {
            await _context.Categories.AddRangeAsync(GetPreconfiguredCategories());
            await _context.SaveChangesAsync();
        }
        if (!await _context.CategoryCourses.AnyAsync())
        {
            await _context.CategoryCourses.AddRangeAsync(GetPreconfiguredCategoryCourses());
            await _context.SaveChangesAsync();
        }
        if (!await _context.CourseStudents.AnyAsync())
        {
            await _context.CourseStudents.AddRangeAsync(GetPreconfiguredCourseStudents());
            await _context.SaveChangesAsync();
        }
        if (!await _context.Users.AnyAsync())
        {
            await _context.Users.AddRangeAsync(GetPreconfiguredUser());
            await _context.SaveChangesAsync();
        }
    }
    static IEnumerable<Course> GetPreconfiguredCourses()
    {
        return new List<Course>
        {
            new Course
            {
                Name = "Course1",
                ShortDescription = "show description course1",
                Description = "description course1",
                ObjetiveAudience = "all",
                Goals = "make you better",
                Requirements = "nothing",
                Syllabus = "course1",
                Level = Models.Enums.Level.Advanced,
                CreatedAt = DateTime.Now
            },
            new Course
            {
                Name = "Course2",
                ShortDescription = "show description course2",
                Description = "description course2",
                ObjetiveAudience = "all",
                Goals = "make you better",
                Requirements = "nothing",
                Syllabus = "course2",
                Level = Models.Enums.Level.Advanced,
                CreatedAt = DateTime.Now
            },
            new Course
            {
                Name = "Course3",
                ShortDescription = "show description course3",
                Description = "description course3",
                ObjetiveAudience = "all",
                Goals = "make you better",
                Requirements = "nothing",
                Syllabus = "course3",
                Level = Models.Enums.Level.Intermediate,
                CreatedAt = DateTime.Now
            },
            new Course
            {
                Name = "Course4",
                ShortDescription = "show description course4",
                Description = "description course4",
                ObjetiveAudience = "all",
                Goals = "make you better",
                Requirements = "nothing",
                Syllabus = "course4",
                Level = Models.Enums.Level.Intermediate,
                CreatedAt = DateTime.Now
            },
            new Course
            {
                Name = "Course5",
                ShortDescription = "show description course5",
                Description = "description course5",
                ObjetiveAudience = "all",
                Goals = "make you better",
                Requirements = "nothing",
                Syllabus = "course5",
                Level = Models.Enums.Level.Entry,
                CreatedAt = DateTime.Now
            },
        };
    }
    static IEnumerable<Student> GetPreconfiguredStudents()
    {
        return new List<Student>
        {
            new Student
            {
                Name = "Pablo",
                LastName = "Perez",
                DateOfBirth = new DateTime(2000,4,15),
                CreatedAt = DateTime.Now
            },
            new Student
            {
                Name = "Claudi",
                LastName = "Alina",
                DateOfBirth = new DateTime(2000,8,25),
                CreatedAt = DateTime.Now
            },
            new Student
            {
                Name = "Cesar",
                LastName = "Soto",
                DateOfBirth = new DateTime(1999,9,9),
                CreatedAt = DateTime.Now
            },
            new Student
            {
                Name = "Angelis",
                LastName = "Medina",
                DateOfBirth = new DateTime(2004,5,5),
                CreatedAt = DateTime.Now
            },
            new Student
            {
                Name = "Fernando",
                LastName = "Pujols",
                DateOfBirth = new DateTime(2006,3,12),
                CreatedAt = DateTime.Now
            },
        };
    }
    static IEnumerable<Chapter> GetPreconfiguredChapters()
    {
        return new List<Chapter>
        {
            new Chapter
            {
                CourseId = 1,
                List = "chapter1",
                CreatedAt = DateTime.Now
            },
            new Chapter
            {
                CourseId = 2,
                List = "chapter2",
                CreatedAt = DateTime.Now
            },
            new Chapter
            {
                CourseId = 3,
                List = "chapter3",
                CreatedAt = DateTime.Now
            },
            new Chapter
            {
                CourseId = 4,
                List = "chapter4",
                CreatedAt = DateTime.Now
            },
            new Chapter
            {
                CourseId = 5,
                List = "chapter5",
                CreatedAt = DateTime.Now
            },
        };
    }
    static IEnumerable<Category> GetPreconfiguredCategories()
    {
        return new List<Category>
        {
            new Category
            {
                Name = "Programming",
                CreatedAt = DateTime.Now
            },
            new Category
            {
                Name = "Economy",
                CreatedAt = DateTime.Now
            },
            new Category
            {
                Name = "Social Media",
                CreatedAt = DateTime.Now
            },
        };
    }
    static IEnumerable<CategoryCourse> GetPreconfiguredCategoryCourses()
    {
        return new List<CategoryCourse>
        {
            new CategoryCourse
            {
                CourseId = 1,
                CategoryId = 1,
            },
            new CategoryCourse
            {
                CourseId = 2,
                CategoryId = 1,
            },
            new CategoryCourse
            {
                CourseId = 3,
                CategoryId = 2,
            },
            new CategoryCourse
            {
                CourseId = 4,
                CategoryId = 3,
            },
            new CategoryCourse
            {
                CourseId = 5,
                CategoryId = 1,
            },
        };
    }
    static IEnumerable<CourseStudent> GetPreconfiguredCourseStudents()
    {
        return new List<CourseStudent>
        {
            new CourseStudent
            {
                CourseId = 1,
                StudentId = 1
            },
            new CourseStudent
            {
                CourseId = 1,
                StudentId = 2
            },
            new CourseStudent
            {
                CourseId = 2,
                StudentId = 2
            },
            new CourseStudent
            {
                CourseId = 2,
                StudentId = 3
            },
            new CourseStudent
            {
                CourseId = 3,
                StudentId = 4
            },
            new CourseStudent
            {
                CourseId = 3,
                StudentId = 5
            },
            new CourseStudent
            {
                CourseId = 4,
                StudentId = 1
            },
            new CourseStudent
            {
                CourseId = 4,
                StudentId = 3
            },
            new CourseStudent
            {
                CourseId = 5,
                StudentId = 2
            },
            new CourseStudent
            {
                CourseId = 5,
                StudentId = 5
            },
        };
    }

    static IEnumerable<User> GetPreconfiguredUser()
    {
        return new List<User>
        {
            new User
            {
                Name = "Admin",
                Email = "admin@hotmail.com",
                Password = "admin.123",
                CreatedAt = DateTime.Now
            },
            new User
            {
                Name = "other",
                Email = "other@hotmail.com",
                Password = "other.123",
                CreatedAt = DateTime.Now
            },
        };
    }
}