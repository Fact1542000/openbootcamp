using System.ComponentModel.DataAnnotations;
namespace UniversityApi.Models.Dtos.Student;

public class PutStudentDto
{
    [Required]
    public string Name { get; set; } = null!;
    [Required]
    public string LastName { get; set; } = null!;
    [Required]
    public DateTime DateOfBirth { get; set; }
    [Required]
    public DateTime UpdatedAt { get; set; }
}