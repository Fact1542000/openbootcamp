namespace UniversityApi.Models;

public class JwtSettings
{
    public bool ValidateIssuserSigningKey { get; set; }
    public string IssuserSigningKey { get; set; } = null!;

    public bool ValidateIssuer { get; set; } = true;
    public string ValidIssuer { get; set; } = null!;

    public bool ValidateAudience { get; set; } = true;
    public string ValidAudience { get; set; } = null!;

    public bool RequireExpirationTime { get; set; }
    public bool ValidateLifeTime { get; set; } = true;
}