using System.ComponentModel.DataAnnotations;
using UniversityApi.Models.Enums;

namespace UniversityApi.Models.Dtos.User;

public class PutUserDto
{
    public string Name { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public Rol Rol { get; set; }
    [EmailAddress]
    public string Email { get; set; } = null!;
    public string Password { get; set; } = null!;
    [Required]
    public DateTime UpdatedAt { get; set; }
}