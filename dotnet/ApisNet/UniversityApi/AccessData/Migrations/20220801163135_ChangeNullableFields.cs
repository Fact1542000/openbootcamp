﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UniversityApi.AccessData.Migrations
{
    public partial class ChangeNullableFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Syllabus",
                table: "Courses",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Syllabus",
                table: "Courses");
        }
    }
}
