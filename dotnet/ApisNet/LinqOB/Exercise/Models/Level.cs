namespace LinqOB.Exercise.Models;

public enum Level
{
    Entry,
    Medium,
    Advanced
}