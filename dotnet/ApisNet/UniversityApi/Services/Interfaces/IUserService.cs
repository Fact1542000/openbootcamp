using UniversityApi.Models;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.User;

namespace UniversityApi.Services.Interfaces;

public interface IUserService
{
    Task<IEnumerable<GetUserDto>> GetAllAsync();
    Task<GetUserDto> GetByIdAsync(int id);

    Task<UserTokens> LoginAsync(UserLoginDto userLoginDto);
    Task<User> AddAsync(PostUserDto userDto);
    Task EditAsync(int id, PutUserDto userDto);
    Task DeleteAsync(int id);
    Task<int> SaveChangeAsync();
}