using System.ComponentModel.DataAnnotations;

namespace UniversityApi.Models.Dtos.Student;

public class PostStudentDto
{
    [Required]
    public string Name { get; set; } = null!;
    [Required]
    public string LastName { get; set; } = null!;
    [Required]
    public DateTime DateOfBirth { get; set; }
    public int CourseId { get; set; } = 0;
    [Required]
    public DateTime CreatedAt { get; set; }
}