using System.ComponentModel.DataAnnotations;
namespace UniversityApi.Models.Dtos.Category;

public class PutCategoryDto
{
    [Required]
    public string Name { get; set; } = null!;
}