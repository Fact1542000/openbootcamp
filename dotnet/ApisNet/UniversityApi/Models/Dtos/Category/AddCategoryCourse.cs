using System.ComponentModel.DataAnnotations;
namespace UniversityApi.Models.Dtos.Category;

public class AddCategoryCourseDto
{
    [Required]
    public int CourseId { get; set; }
}