using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UniversityApi.AccessData;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Dtos.Student;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Services;

public class StudentService : IStudentService
{
    private readonly UniversityDbContext _db;
    private readonly IMapper _mapper;
    private readonly ILogger<StudentService> _logger;

    public StudentService(UniversityDbContext db, IMapper mapper, ILogger<StudentService> logger)
    {
        _db = db;
        _mapper = mapper;
        _logger = logger;
    }

    #region READ
    public async Task<IEnumerable<GetStudentDto>> GetAllWithoutEnrolledCourseAsync()
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.GetAllWithoutEnrolledCourseAsync)} - Called");
        var IdsStudentEnrolled = await _db.CourseStudents.Select(cs => cs.StudentId).ToListAsync();
        var students = await GetAllAsync();

        return students.Where(student => IdsStudentEnrolled.All(se => se != student.Id));
    }

    public async Task<IEnumerable<CourseStudent>> GetAllCourseStudentAsync()
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.GetAllCourseStudentAsync)} - Called");
        return await _db.CourseStudents.Include(cs => cs.Course).Include(cs => cs.Student).ToListAsync();
    }

    public async Task<IEnumerable<GetCourseDto>> GetStudentCoursesAsync(int studentId)
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.GetStudentCoursesAsync)} - Called");
        return await _db.CourseStudents.Include(cs => cs.Course).Where(cs => cs.StudentId == studentId).Select(cs => _mapper.Map<GetCourseDto>(cs.Course)).ToListAsync();
    }

    public async Task<IEnumerable<GetStudentDto>> GetAllAsync()
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.GetAllAsync)} - Called");
        return await _db.Students.Select(s => _mapper.Map<GetStudentDto>(s)).ToListAsync();
    }
    public async Task<GetStudentDto> GetByIdAsync(int id)
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.GetByIdAsync)} - Called");
        var student = await _db.Students.FindAsync(id);
        if (student is null) return null!;
        return _mapper.Map<GetStudentDto>(student);
    }
    public async Task<IEnumerable<GetStudentDto>> GetAllStudentWithCoursesAsync()
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.GetAllStudentWithCoursesAsync)} - Called");
        var students = await GetAllAsync();

        var studentsDto = students.Select(s => _mapper.Map<GetStudentDto>(s)).ToList();

        studentsDto.ForEach(s => s.Courses = _db.CourseStudents.Include(cs => cs.Course)
                                                                .Where(cs => cs.StudentId == s.Id)
                                                                .Select(cs => cs.Course.Name)
                                                                .ToList());
        return studentsDto;
    }
    #endregion

    #region WRITE
    public async Task<Student> AddAsync(PostStudentDto studentDto)
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.AddAsync)} - Called");
        var student = _mapper.Map<Student>(studentDto);
        await _db.Students.AddAsync(student);
        if (await SaveChangeAsync() < 1) return null!;
        if (studentDto.CourseId > 0)
        {
            var courseStudent = new CourseStudent
            {
                CourseId = studentDto.CourseId,
                StudentId = student.Id
            };
            await _db.CourseStudents.AddAsync(courseStudent);
        }
        if (await SaveChangeAsync() < 1) return null!;
        return student;
    }
    public async Task EditAsync(int id, PutStudentDto studentDto)
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.EditAsync)} - Called");
        var studentToEdit = await _db.Students.FindAsync(id);
        _mapper.Map(studentDto, studentToEdit);
    }
    public async Task DeleteAsync(int id)
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.DeleteAsync)} - Called");
        var studentToDelete = await _db.Students.FirstOrDefaultAsync(s => s.Id == id);
        _db.Remove(studentToDelete!);
    }

    public async Task AddCourseAsync(int id, AddCourseStudentDto courseStudentDto)
    {
        _logger.LogInformation($"{nameof(StudentService)}::{nameof(this.AddCourseAsync)} - Called");
        var courseStudent = new CourseStudent
        {
            StudentId = id,
            CourseId = courseStudentDto.CourseId
        };
        await _db.CourseStudents.AddAsync(courseStudent);
    }

    public async Task<int> SaveChangeAsync()
    {
        return await _db.SaveChangesAsync();
    }
    #endregion
}