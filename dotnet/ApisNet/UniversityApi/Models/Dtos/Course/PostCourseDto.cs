using System.ComponentModel.DataAnnotations;
using UniversityApi.Models.Enums;

namespace UniversityApi.Models.Dtos.Course;

public class PostCourseDto
{
    [Required]
    public string Name { get; set; } = null!;
    [Required]
    public string ShortDescription { get; set; } = null!;
    [Required]
    public string Description { get; set; } = null!;
    [Required]
    public string ObjetiveAudience { get; set; } = null!;
    [Required]
    public string Goals { get; set; } = null!;
    [Required]
    public string Requirements { get; set; } = null!;
    [Required]
    public string Syllabus { get; set; } = null!;
    [Required]
    public Level Level { get; set; }
    public int CategoryId { get; set; } = 0;
    public int StudentId { get; set; } = 0;
}