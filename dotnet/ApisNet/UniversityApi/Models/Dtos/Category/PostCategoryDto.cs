using System.ComponentModel.DataAnnotations;
namespace UniversityApi.Models.Dtos.Category;

public class PostCategoryDto
{
    [Required]
    public string Name { get; set; } = null!;
    [Required]
    public DateTime CreatedAt { get; set; }
    public int CourseId { get; set; }
}