using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace FakeStoreApiVersionControl;

public class SwaggerOptionsConfiguration : IConfigureNamedOptions<SwaggerGenOptions>
{
    private readonly IApiVersionDescriptionProvider _provider;

    public SwaggerOptionsConfiguration(IApiVersionDescriptionProvider provider)
    {
        _provider = provider;
    }

    public void Configure(string name, SwaggerGenOptions options)
    {
        Configure(options);
    }

    public void Configure(SwaggerGenOptions options)
    {
        foreach (var description in _provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(description.GroupName, CreateVersionInfo(description));
        }
    }
    private static OpenApiInfo CreateVersionInfo(ApiVersionDescription description)
    {
        var info = new OpenApiInfo()
        {
            Title = "My .Net API restful",
            Version = description.ApiVersion.ToString(),
            Description = "This is my second API Versioning control",
            Contact = new OpenApiContact()
            {
                Email = "francisalcantara001@gmail.com",
                Name = "Francis"
            }
        };

        if (description.IsDeprecated)
        {
            info.Description += "This API Version has been deprecated";
        }
        return info;
    }
}
