namespace UniversityApi.Models.DataModels;

public class Chapter : BaseEntity
{
    public int CourseId { get; set; }
    public Course Course { get; set; } = null!;
    public string List { get; set; } = string.Empty;
}