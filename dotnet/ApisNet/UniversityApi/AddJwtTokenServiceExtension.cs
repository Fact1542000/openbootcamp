using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using UniversityApi.Models;

namespace UniversityApi;

public static class AddJwtTokenServiceExtension
{
    public static void AddJwtTokenService(this IServiceCollection services, IConfiguration configuration)
    {
        var BindJwtSettings = new JwtSettings();
        configuration.Bind("JsonWebTokenKeys", BindJwtSettings);

        services.AddSingleton(BindJwtSettings);

        services.AddAuthentication(options => {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options => {
            options.RequireHttpsMetadata = false;
            options.SaveToken = true;
            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = BindJwtSettings.ValidateIssuserSigningKey,
                IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(BindJwtSettings.IssuserSigningKey)),
                ValidateIssuer = BindJwtSettings.ValidateIssuer,
                ValidIssuer = BindJwtSettings.ValidIssuer,
                ValidAudience = BindJwtSettings.ValidAudience,
                ValidateAudience = BindJwtSettings.ValidateAudience,
                RequireExpirationTime = BindJwtSettings.RequireExpirationTime,
                ValidateLifetime = BindJwtSettings.ValidateLifeTime,
                ClockSkew = TimeSpan.FromDays(1)
            };
        });
    }
}