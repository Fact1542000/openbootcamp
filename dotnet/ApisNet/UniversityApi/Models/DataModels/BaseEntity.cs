using System.ComponentModel.DataAnnotations.Schema;

namespace UniversityApi.Models.DataModels;
public class BaseEntity
{
    public int Id { get; set; }

    public int? CreatedByUserId { get; set; }
    [ForeignKey("CreatedByUserId")]
    public User? CreatedBy { get; set; }
    public DateTime CreatedAt { get; set; } = DateTime.Now;

    public int? UpdatedByUserId { get; set; }
    [ForeignKey("UpdatedByUserId")]
    public User? UpdatedBy { get; set; }
    public DateTime? UpdatedAt { get; set; }

    public int? DeletedByUserId { get; set; }
    [ForeignKey("DeletedByUserId")]
    public User? DeletedBy { get; set; }
    public DateTime? DeletedAt { get; set; }

    public bool IsDeleted { get; set; }
}