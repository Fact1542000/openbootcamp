using GlobalApi.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace GlobalApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PostsController : ControllerBase
{
    private readonly IStringLocalizer<PostsController> _stringLocalizer;
    private readonly IStringLocalizer<SharedResource> _sharedResourcesLocalizer;

    public PostsController(IStringLocalizer<PostsController> stringLocalizer, IStringLocalizer<SharedResource> sharedResourcesLocalizer)
    {
        _stringLocalizer = stringLocalizer;
        _sharedResourcesLocalizer = sharedResourcesLocalizer;
    }

    [HttpGet]
    [Route("PostControllerResource")]
    public IActionResult GetUsingPostControllerResource()
    {
        // Find text
        var article = _stringLocalizer["Article"];
        var postName = _stringLocalizer.GetString("Welcome").Value ?? string.Empty;

        return Ok(new {
            PostType = article.Value,
            PostName = postName
        });
    }

    [HttpGet]
    [Route("SharedResource")]
    public IActionResult GetUsingSharedResource()
    {
        var article = _sharedResourcesLocalizer["Article"];
        var postName = _sharedResourcesLocalizer.GetString("Welcome").Value ?? string.Empty;
        var todayIs = string.Format(_sharedResourcesLocalizer.GetString("TodayIs"), DateTime.Now.ToLongDateString());

        return Ok(new
        {
            PostType = article.Value,
            PostName = postName,
            TodayIs = todayIs,
        });
    }
}