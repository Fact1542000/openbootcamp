using System.Text.Json;
using FakeStoreApiVersionControl.Dtos.V2;
using Microsoft.AspNetCore.Mvc;

namespace FakeStoreApiVersionControl.Controllers.V2;

[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiController]
public class ProductController : ControllerBase
{
    private const string ApiUrl = "https://fakestoreapi.com/products";
    private readonly HttpClient _httpClient;
    public ProductController(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    [HttpGet(Name = "GetAllProductAsync")]
    [MapToApiVersion("2.0")]
    public async Task<IActionResult> GetAllProductAsync()
    {
        var response = await _httpClient.GetStreamAsync(ApiUrl);
        var productResponse = await JsonSerializer.DeserializeAsync<Product[]>(response);
        for (int i = 0; i < productResponse!.Length; i++)
        {
            productResponse[i].InternalId = Guid.NewGuid();
        }
        return Ok(productResponse);
    }
}