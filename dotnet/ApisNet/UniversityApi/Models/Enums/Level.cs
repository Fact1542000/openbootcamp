namespace UniversityApi.Models.Enums;
public enum Level
{
    Entry,
    Intermediate,
    Advanced
}