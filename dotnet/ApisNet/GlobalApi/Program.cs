var builder = WebApplication.CreateBuilder(args);
{
    // LOCALIZATION
    builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");

    builder.Services.AddControllers();
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
}

var app = builder.Build();
{
    // SUPPORTED CULTURES
    var supportedCultures = new [] { "en-US", "es-ES", };
    var localizationOptions = new RequestLocalizationOptions()
                            .SetDefaultCulture(supportedCultures[0]) // English by default
                            .AddSupportedCultures(supportedCultures) // Add all supported cultures
                            .AddSupportedUICultures(supportedCultures); // Add supported cultures to UI

    app.UseRequestLocalization(localizationOptions);

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}