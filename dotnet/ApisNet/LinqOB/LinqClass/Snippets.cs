namespace LinqOB.LinqClass;
public static class Snippets
{
    public static void BasicLinq()
    {
        string[] Cars = {
            "VW Golf",
            "VW California",
            "Audi A3",
            "Audi A5",
            "Fiat Punto",
            "Seat Ibiza",
            "Seat Leon"
        };

        // 1. SELECT * FROM Cars
        var carList = from car in Cars select car;

        foreach(var car in carList)
        {
            System.Console.WriteLine(car);
        }

        // 2. SELECT WHERE
        var audis = from car in Cars where car.Contains("Audi") select car;
        foreach(var audi in audis)
        {
            System.Console.WriteLine(audi);
        }
    }

    public static void LinqNumbers()
    {
        List<int> NumbersList = new() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        var orderedNumbers = NumbersList.Select(num => num * 3)
                                        .Where(num => num != 9)
                                        .OrderBy(num => num);
    }

    public static void SearchExamples()
    {
        List<string> textList = new()
        {
            "a",
            "bx",
            "c",
            "d",
            "e",
            "cj",
            "f",
            "c"
        };

        // 1. First of all elements
        var first = textList.First(); // "a"

        // 2. First element that is "c"
        var cText = textList.First(text => text.Equals("c")); // "c"

        // 4. First element that contains "j"
        var jText = textList.First(text => text.Contains('j')); // "cj"

        // 5. First element that contains "z" or default
        var zText = textList.FirstOrDefault(text => text.Contains("z")); // Exception

        // 6. Last element that contains "z" or defualt
        var lastZText = textList.LastOrDefault(text => text.Contains("z")); // ""

        // 7. Singles values
        var uniqueText = textList.Single();
        var uniqueOrDefault = textList.SingleOrDefault();

        int[] evenNumbers = { 0, 2, 4, 6, 8};
        int[] othersEvenNumbers = { 0, 2, 6 };

        var myEvenNumbers = evenNumbers.Except(othersEvenNumbers); // { 4, 8 }
    }

    public static void MultipleSelects()
    {
        // SELECT MANY
        string[] myOpinions =
        {
            "Opinion 1, text 1",
            "Opinion 2, text 2",
            "Opinion 3, text 3",
        };

        var myOpinionSelection = myOpinions.SelectMany(opinion => opinion.Split(','));

        var enterprises = new[]
        {
            new Enterprise()
            {
                Id = 1,
                Name = "enterprise 1",
                Employees = new Employee[] {
                    new Employee {
                        Id = 1,
                        Name = "employee 1",
                        Email = "employee1@gmail.com",
                        Salary = 3000
                    },
                    new Employee {
                        Id = 2,
                        Name = "employee 2",
                        Email = "employee2@gmail.com",
                        Salary = 5000
                    },
                    new Employee {
                        Id = 3,
                        Name = "employee 3",
                        Email = "employee3@gmail.com",
                        Salary = 1000
                    },
                }
            },
            new Enterprise
            {
                Id = 2,
                Name = "enterprise 2",
                Employees = new Employee[]
                {
                    new Employee {
                        Id = 4,
                        Name = "employee 4",
                        Email = "employee4@gmail.com",
                        Salary = 2000
                    },
                    new Employee {
                        Id = 5,
                        Name = "employee 5",
                        Email = "employee5@gmail.com",
                        Salary = 3000
                    },
                    new Employee {
                        Id = 6,
                        Name = "employee 6",
                        Email = "employee6@gmail.com",
                        Salary = 1500
                    }
                }
            }
        };

        // Obtain all Employees of all enterprise
        var employeeList = enterprises.SelectMany(e => e.Employees!);

        // Know if list is empty
        bool hasEnterprise = enterprises.Any();
        bool hasEmployee = enterprises.Any(e => e.Employees!.Any());

        // All enterprise at least has an employee with more than 1000 of salary
        var hasEmployeeWithSalaryMoreThan1000 = enterprises.Any(e => e.Employees!.Any(e => e.Salary > 1000));
    }

    public static void LinqCollections()
    {
        var firstList = new List<string>() { "a", "b", "c" };
        var secondList = new List<string>() { "a", "c", "d" };

        // INNER JOIN
        var commonResult = from firstElement in firstList
                            join secondElement in secondList
                            on firstElement equals secondElement
                            select new { firstElement, secondElement};

        var commonResult2 = firstList.Join(
            secondList,
            element => element,
            secondElement => secondElement,
            (element, secondElement) => new { element, secondElement}
        );

        // OUTER JOIN - LEFT
        var leftOuterJoin = from element in firstList
                            join secondElement in secondList
                            on element equals secondElement
                            into temporalList
                            from temporalElement in temporalList.DefaultIfEmpty()
                            where element != temporalElement
                            select new {Element = element};

        var leftOuterJoin2 = from element in firstList
                            from secondElement in secondList.Where(s => s == element).DefaultIfEmpty()
                            select new { Element = element, SecondElement = secondElement};

        // OUTER JOIN - RIGHT
        var rightOuterJoin = from secondElement in secondList
                            join element in firstList
                            on secondElement equals element
                            into temporalList
                            from temporalElement in temporalList.DefaultIfEmpty()
                            where secondElement != temporalElement
                            select new { Element = secondElement};

        // Union
        var unionList = leftOuterJoin.Union(rightOuterJoin);
    }

    public static void SkipTakeLinq()
    {
        // SKIP
        var myList = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, };

        var skipTwoFirstValues = myList.Skip(2); // { 3, 4, 5, 6, 7, 8, 9, }
        var skipTwoLastValues = myList.SkipLast(2); // { 1, 2, 3, 4, 5, 6, 7 }
        var skipWhileSmallerThan4 = myList.SkipWhile(num => num < 4); // { 5, 6, 7, 8, 9, }

        // TAKE
        var takeFirsTwoValues = myList.Take(2); // { 1, 2 }
        var takeLastTwoValues = myList.TakeLast(2); // { 8, 9 }
        var takeWhileSmallerThan4 = myList.TakeWhile(num => num < 4); // { 1, 2, 3 }
    }

    // Paging with skip & take
    public static IEnumerable<T> GetPage<T>(IEnumerable<T> collection, int pageNumber, int resultsPerpage)
    {
        var startIndex = (pageNumber - 1) * resultsPerpage;
        return collection.Skip(startIndex).Take(resultsPerpage);
    }

    // Variables
    public static void LinqVariable()
    {
        var numbers = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        var aboveAverage = from number in numbers
                            let average = numbers.Average()
                            let nSquared = Math.Pow(number, 2)
                            where nSquared > average
                            select number;
    }

    // Zip
    public static void ZipLinq()
    {
        var numbers = new[] { 1, 2, 3, 4, 5 };
        var stringNumbers = new[] { "one", "two", "three", "four", "five" };

        IEnumerable<string> zipNumbers = numbers.Zip(stringNumbers, (number, word) => number + "=" + word);
    }

    // Repeat & Range
    public static void RepeatRangeLinq()
    {
        var first1000 = Enumerable.Range(1, 1000);

        var fiveXs = Enumerable.Repeat("X", 5);
    }

    static public void StudentsLinq()
    {
        var classRoom = new[]
        {
            new Student
            {
                Id = 1,
                Name = "Francis",
                Grade = 99,
                Certified = true
            },
            new Student
            {
                Id = 2,
                Name = "Jennifer",
                Grade = 98,
                Certified = true
            },
            new Student
            {
                Id = 3,
                Name = "Emnny",
                Grade = 100,
                Certified = true
            },
            new Student
            {
                Id = 4,
                Name = "Pablo",
                Grade = 40,
                Certified = false
            },
            new Student
            {
                Id = 5,
                Name = "Juan",
                Grade = 10,
                Certified = false
            },
        };

        var certifiedStudent = classRoom.Where(c => c.Certified);

        var notCertifiedStudent = classRoom.Where(c => !c.Certified);

        var aprovedStudentNames = classRoom.Where(c => c.Grade >= 50 && c.Certified).Select(c => c.Name);
    }

    // All
    public static void AllLinq()
    {
        var numbers = new[] { 1, 2, 3, 4, 5 };
        var allAreSmallerThan10 = numbers.All(num => num < 10); // true
        var allAreBiggerOrEqualThan2 = numbers.All(num => num >= 2); // false
    }

    // Aggregate
    public static void AggregateQueries()
    {
        var numbers = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        var sum = numbers.Aggregate((prevsum, current) => prevsum + current);
    }

    // Distinct
    public static void DistinctValues()
    {
        var numbers = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 9, 8 };
        var distinctValues = numbers.Distinct();
    }

    // GroupBy
    public static void GroupByExample()
    {
        List<int> numbers = new() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        // Obtain only even numbers and generate two groups
        var grouped = numbers.GroupBy(num => num % 2 == 0);
        // we will have two groups:
        // 1. The group that doesnt fit the condition (odd numbers)
        // 2. The group that fits the condition (even numbers)
        

        var classRoom = new[]
        {
            new Student
            {
                Id = 1,
                Name = "Francis",
                Grade = 99,
                Certified = true
            },
            new Student
            {
                Id = 2,
                Name = "Jennifer",
                Grade = 98,
                Certified = true
            },
            new Student
            {
                Id = 3,
                Name = "Emnny",
                Grade = 100,
                Certified = true
            },
            new Student
            {
                Id = 4,
                Name = "Pablo",
                Grade = 40,
                Certified = false
            },
            new Student
            {
                Id = 5,
                Name = "Juan",
                Grade = 10,
                Certified = false
            },
        };
        var certifiedQuery = classRoom.GroupBy(student => student.Certified && student.Grade >= 50);
        // we obtain two groups:
        // 1. Not certified student
        // 2. Certfied student
        foreach(var group in certifiedQuery)
        {
            Console.WriteLine("------------------");
            foreach(var student in group)
            {
                Console.WriteLine(student.Name);
            }
        }
    }

    public static void RelationsLinq()
    {
        List<Post> posts = new()
        {
            new Post
            {
                Id = 1,
                Title = "My first post",
                Content = "My first content",
                Created = DateTime.Now,
                Comments = new()
                {
                    new Comment
                    {
                        Id = 1,
                        Title = "My first comment",
                        Content = "My first content",
                        Created = DateTime.Now
                    },
                    new Comment
                    {
                        Id = 2,
                        Title = "My second comment",
                        Content = "My second content",
                        Created = DateTime.Now
                    }
                }
            },
            new Post
            {
                Id = 2,
                Title = "My second post",
                Content = "My second content",
                Created = DateTime.Now,
                Comments = new()
                {
                    new Comment
                    {
                        Id = 3,
                        Title = "other comment",
                        Content = "other content",
                        Created = DateTime.Now
                    },
                    new Comment
                    {
                        Id = 4,
                        Title = "other2 comment",
                        Content = "other2 content",
                        Created = DateTime.Now
                    }
                }
            }
        };

        var commentWithContent = posts.SelectMany(post => post.Comments,
                                                  (post, comment) => new { PostId = post.Id, CommentContext = comment.Content });
    }
}