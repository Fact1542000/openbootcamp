using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UniversityApi.AccessData;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Category;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Services;

public class CategoryService : ICategoryService
{
    private readonly UniversityDbContext _db;
    private readonly IMapper _mapper;
    private readonly ILogger<CategoryService> _logger;

    public CategoryService(UniversityDbContext db, IMapper mapper, ILogger<CategoryService> logger)
    {
        _db = db;
        _mapper = mapper;
        _logger = logger;
    }

    #region READ
    public async Task<IEnumerable<GetCategoryDto>> GetAllAsync()
    {
        _logger.LogInformation($"{nameof(CategoryService)}::{nameof(this.GetAllAsync)} - Called");
        var categories = await _db.Categories.Select(c => _mapper.Map<GetCategoryDto>(c)).ToListAsync();
        var categoryCourses = await _db.CategoryCourses.Include(cc => cc.Course).ToListAsync();
        categories.ForEach(c => c.Courses = categoryCourses.Where(cc => cc.CategoryId == c.Id).Select(cc => _mapper.Map<GetCourseDto>(cc.Course)).ToList());
        return categories;
    }

    public async Task<IEnumerable<GetCourseDto>> GetAllCourseRelatedWithCategory(int id)
    {
        _logger.LogInformation($"{nameof(CategoryService)}::{nameof(this.GetAllCourseRelatedWithCategory)} - Called");
        return await _db.CategoryCourses.Include(cc => cc.Course).Where(cc => cc.CategoryId == id).Select(cc => _mapper.Map<GetCourseDto>(cc.Course)).ToListAsync();
    }

    public async Task<GetCategoryDto> GetByIdAsync(int id)
    {
        _logger.LogInformation($"{nameof(CategoryService)}::{nameof(this.GetByIdAsync)} - Called");
        var category = await _db.Categories.FirstOrDefaultAsync(c => c.Id == id);
        return _mapper.Map<GetCategoryDto>(category);
    }
    #endregion

    #region WRITE
    public async Task<Category> AddAsync(PostCategoryDto categoryDto)
    {
        _logger.LogInformation($"{nameof(CategoryService)}::{nameof(this.AddAsync)} - Called");
        var category = _mapper.Map<Category>(categoryDto);

        await _db.AddAsync(category);
        await SaveChangeAsync();
        if (categoryDto.CourseId > 0)
        {
            var categoryCourse = new CategoryCourse
            {
                CategoryId = category.Id,
                CourseId = categoryDto.CourseId
            };
            await _db.CategoryCourses.AddAsync(categoryCourse);
            await SaveChangeAsync();
        }
        return category;
    }

    public async Task AddCourse(int id, AddCategoryCourseDto courseDto)
    {
        _logger.LogInformation($"{nameof(CategoryService)}::{nameof(this.AddCourse)} - Called");
        var categoryCourse = new CategoryCourse
        {
            CategoryId = id,
            CourseId = courseDto.CourseId
        };
        await _db.CategoryCourses.AddAsync(categoryCourse);
    }

    public async Task DeleteAsync(int id)
    {
        _logger.LogInformation($"{nameof(CategoryService)}::{nameof(this.DeleteAsync)} - Called");
        var category = await _db.Categories.FindAsync(id);
        _db.Categories.Remove(category!);
    }

    public async Task EditAsync(int id, PutCategoryDto categoryDto)
    {
        _logger.LogInformation($"{nameof(CategoryService)}::{nameof(this.EditAsync)} - Called");
        var category = await _db.Categories.FindAsync(id);
        _mapper.Map(categoryDto, category);
    }

    public async Task<int> SaveChangeAsync()
    {
        return await _db.SaveChangesAsync();
    }
    #endregion
}