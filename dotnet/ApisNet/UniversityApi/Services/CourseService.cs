using System.Runtime.InteropServices;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UniversityApi.AccessData;
using UniversityApi.Models.DataModels;
using UniversityApi.Models.Dtos.Course;
using UniversityApi.Models.Dtos.Student;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Services;

public class CourseService : ICourseService
{
    private readonly UniversityDbContext _db;
    private readonly IMapper _mapper;
    private readonly ILogger<CourseService> _logger;

    public CourseService(UniversityDbContext db, IMapper mapper, ILogger<CourseService> logger)
    {
        _db = db;
        _mapper = mapper;
        _logger = logger;
    }

    #region READ

    public async Task<IEnumerable<GetCourseDto>> GetAllAsync()
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetAllAsync)} - Called");
        return await _db.Courses.Select(c => _mapper.Map<GetCourseDto>(c)).ToListAsync();
    }

    public async Task<IEnumerable<GetCourseDto>> GetAllCoursesWithStudents()
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetAllCoursesWithStudents)} - Called");
        var courses = await _db.Courses.ToListAsync();
        var coursesDto = courses.ConvertAll(c => _mapper.Map<GetCourseDto>(c));

        coursesDto.ForEach(c => c.Students = _db.CourseStudents.Include(cs => cs.Student)
                                                                .Where(cs => cs.CourseId == c.Id)
                                                                .Select(cs => $"{cs.Student.Name} {cs.Student.LastName}")
                                                                .ToList());
        return coursesDto;
    }

    public async Task<IEnumerable<GetCourseDto>> GetAllCourseWithAllField()
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetAllCourseWithAllField)} - Called");
        var courses = await GetAllAsync();
        var categoriesCourses = await _db.CategoryCourses.Include(cc => cc.Category).ToListAsync();
        var studentsCourses = await _db.CourseStudents.Include(cs => cs.Student).ToListAsync();
        var chapterCourse = await _db.Chapters.ToListAsync();

        courses.ToList().ForEach(c => {
            c.Categories = categoriesCourses.Where(cc => cc.CourseId == c.Id).Select(cc => cc.Category.Name).ToList();
            c.Students = studentsCourses.Where(sc => sc.CourseId == c.Id).Select(cs => cs.Student.Name+" "+cs.Student.LastName).ToList();
            c.Chapter = chapterCourse.Where(cc => cc.CourseId == c.Id).Select(cc => cc.List).ElementAtOrDefault(0) ?? string.Empty;
        });

        return courses;
    }

    public async Task<IEnumerable<GetCourseDto>> GetAllCourseWithoutSyllabus()
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetAllCourseWithoutSyllabus)} - Called");
        return await _db.Courses.Where(c => c.Syllabus.Length == 0)
                                .Select(c => _mapper.Map<GetCourseDto>(c))
                                .ToListAsync();
    }

    public async Task<IEnumerable<GetCourseDto>> GetAllCourseWithSpecificCategory(int categoryId)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetAllCourseWithSpecificCategory)} - Called");
        return await _db.CategoryCourses.Include(cc => cc.Course).Where(cc => cc.CategoryId == categoryId).Select(cc => _mapper.Map<GetCourseDto>(cc.Course)).ToListAsync();
    }

    public async Task<GetCourseDto> GetByIdAsync(int id)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetByIdAsync)} - Called");
        var course = await _db.Courses.FirstOrDefaultAsync(c => c.Id == id);
        return _mapper.Map<GetCourseDto>(course!);
    }

    public async Task<GetCourseDto> GetCourseWithCategory(int id)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetCourseWithCategory)} - Called");
        var categories = await _db.CategoryCourses.Include(cc => cc.Category).Where(cc => cc.CourseId == id).Select(cc => cc.Category).ToListAsync();
        var courseDb = await GetByIdAsync(id);
        courseDb.Categories = categories.ConvertAll(c => c.Name);
        return courseDb;
    }

    public async Task<GetCourseDto> GetCourseWithChapter(int id)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetCourseWithChapter)} - Called");
        var chapter = await _db.Chapters.FirstOrDefaultAsync(c => c.CourseId == id);
        var courseDb = await GetByIdAsync(id);
        courseDb.Chapter = chapter!.List;
        return courseDb;
    }

    public async Task<IEnumerable<GetStudentDto>> GetStudentsFromSpecificCourse(int courseId)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetStudentsFromSpecificCourse)} - Called");
        return await _db.CourseStudents.Include(cs => cs.Student).Where(cs => cs.CourseId == courseId).Select(cs => _mapper.Map<GetStudentDto>(cs.Student)).ToListAsync();
    }

    public async Task<string> GetSyllabusFormSpecificCourse(int courseId)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.GetSyllabusFormSpecificCourse)} - Called");
        var course = await _db.Courses.FindAsync(courseId);
        return course!.GetSyllabus();
    }
    #endregion

    #region WRITE
    public async Task<Course> AddAsync(PostCourseDto courseDto)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.AddAsync)} - Called");
        var course = _mapper.Map<Course>(courseDto);

        await _db.Courses.AddAsync(course);

        var result = await SaveChangeAsync();

        if (result <= 0) return null!;

        if (courseDto.StudentId > 0)
        {
            var courseStudent = new CourseStudent
            {
                CourseId = course.Id,
                StudentId = courseDto.StudentId
            };
            await _db.CourseStudents.AddAsync(courseStudent);
        }

        if (courseDto.CategoryId > 0)
        {
            var categoryCourse = new CategoryCourse
            {
                CourseId = course.Id,
                CategoryId = courseDto.CategoryId
            };
            await _db.CategoryCourses.AddAsync(categoryCourse);
        }

        if (await SaveChangeAsync() < 1) return null!;
        return course;
    }

    public async Task AddStudentAsync(int courseId, AddStudentCourseDto addStudentCourseDto)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.AddStudentAsync)} - Called");
        var courseStudent = new CourseStudent
        {
            CourseId = courseId,
            StudentId = addStudentCourseDto.StudentId
        };
        await _db.CourseStudents.AddAsync(courseStudent);
    }

    public async Task DeleteAsync(int id)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.DeleteAsync)} - Called");
        var course = await _db.Courses.FindAsync(id);
        var courseStudentToDelete = await _db.CourseStudents.Where(cs => cs.CourseId == id).ToListAsync();
        _db.CourseStudents.RemoveRange(courseStudentToDelete);
        _db.Courses.Remove(course!);
    }

    public async Task EditAsync(int id, PutCourseDto courseDto)
    {
        _logger.LogInformation($"{nameof(CourseService)}::{nameof(this.EditAsync)} - Called");
        var courseToEdit = await _db.Courses.FindAsync(id);
        _mapper.Map(courseDto, courseToEdit);
    }

    public async Task<int> SaveChangeAsync()
    {
        return await _db.SaveChangesAsync();
    }
    #endregion
}