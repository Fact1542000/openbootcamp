namespace UniversityApi.Models.Dtos.Chapter;

public class PutChapterDto
{
    public int? CourseId { get; set; }
    public string List { get; set; } = null!;
    public DateTime UpdatedAt { get; set; }
}