namespace UniversityApi.Models.Dtos.Student;

public class GetStudentDto
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public DateTime DateOfBirth { get; set; }
    public List<string> Courses { get; set; } = new();
}