using System.Reflection;
using Microsoft.EntityFrameworkCore;
using UniversityApi.Models.DataModels;

namespace UniversityApi.AccessData;
public class UniversityDbContext : DbContext
{
    private readonly ILoggerFactory _loggerFactory;
    public UniversityDbContext(DbContextOptions<UniversityDbContext> options, ILoggerFactory loggerFactory) : base(options)
    {
        _loggerFactory = loggerFactory;
    }

    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Course> Courses { get; set; } = null!;
    public DbSet<Category> Categories { get; set; } = null!;
    public DbSet<Chapter> Chapters { get; set; } = null!;
    public DbSet<Student> Students { get; set; } = null!;
    public DbSet<CategoryCourse> CategoryCourses { get; set; } = null!;
    public DbSet<CourseStudent> CourseStudents { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var logger = _loggerFactory!.CreateLogger<UniversityDbContext>();
        // optionsBuilder.LogTo(d => logger.Log(LogLevel.Information, d, new [] { DbLoggerCategory.Database.Name }));
        // optionsBuilder.EnableSensitiveDataLogging();
        optionsBuilder.LogTo(d => logger.Log(LogLevel.Information, d, new [] { DbLoggerCategory.Database.Name }), LogLevel.Information)
                            .EnableSensitiveDataLogging()
                            .EnableDetailedErrors();
    }
}