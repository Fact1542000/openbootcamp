using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UniversityApi.Models.DataModels;

namespace UniversityApi.AccessData.Configs;

public class StudentConfiguration : IEntityTypeConfiguration<Student>
{
    public void Configure(EntityTypeBuilder<Student> builder)
    {
        builder.HasKey(s => s.Id);

        builder.Property(s => s.Name).IsRequired();
        builder.Property(s => s.LastName).IsRequired();
        builder.Property(s => s.DateOfBirth).IsRequired();
    }
}
