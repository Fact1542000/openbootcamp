namespace LinqOB.Exercise.Models;

public class Course
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public Level Level { get; set; }
    public List<Category>? Category { get; set; }
    public List<Student>? Students { get; set; }
}