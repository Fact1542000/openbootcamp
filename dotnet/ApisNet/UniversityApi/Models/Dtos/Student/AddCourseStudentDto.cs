using System.ComponentModel.DataAnnotations;
namespace UniversityApi.Models.Dtos.Student;

public class AddCourseStudentDto
{
    [Required]
    public int CourseId { get; set; }
}