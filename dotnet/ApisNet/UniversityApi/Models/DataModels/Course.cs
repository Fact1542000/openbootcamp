using UniversityApi.Models.Enums;

namespace UniversityApi.Models.DataModels;
public class Course : BaseEntity
{
    public string Name { get; set; } = string.Empty;
    public string ShortDescription { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string ObjetiveAudience { get; set; } = string.Empty;
    public string Goals { get; set; } = string.Empty;
    public string Requirements { get; set; } = string.Empty;
    public string Syllabus { get; set; } = string.Empty;
    public Level Level { get; set; }
    public IList<CategoryCourse> CategoryCourses { get; set; } = null!;
    public IList<CourseStudent> CourseStudents { get; set; } = null!;
    public Chapter Chapter { get; set; } = null!;

    public string GetSyllabus()
    {
        return this.Syllabus;
    }
}